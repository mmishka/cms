// DINAMIC TIME

var UsAgentLang = (navigator.language || navigator.systemLanguage || navigator.userLanguage).substr(0, 2).toLowerCase();
// Вибираем нужную локализацию.
switch (UsAgentLang) {
    case 'ru' :
        LangAgo = 'назад';
        LangAfter = 'через';
        LangNameMonths = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августa', 'Сентября', 'Октября', 'Ноября', 'Декабря'];
        LangNameMonthsMin = ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'];
        LangNameWeekdays = ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'];
        LangNameWeekdaysMin = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];
        LangDimensionTime = {
                'n' : ['месяцев', 'месяц', 'месяца', 'месяц'],
                'j' : ['дней', 'день', 'дня'],
                'G' : ['часов', 'час', 'часа'],
                'i' : ['минут', 'минуту', 'минуты'],
                's' : ['секунд', 'секунду', 'секунды']
        }
        break;
    default:
        LangAgo = 'ago';
        LangAfter = 'after';
        LangNameMonths = ['January', 'February', 'Marth', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        LangNameMonthsMin = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        LangNameWeekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        LangNameWeekdaysMin = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        LangDimensionTime = {
                'n' : ['months', 'month', 'months'],
                'j' : ['days', 'day', 'days'],
                'G' : ['hours', 'h', 'hours'],
                'i' : ['minutes', 'minute', 'minutes'],
                's' : ['seconds', 'second', 'seconds']
        }
        break;
}

// Выводит элемент даты с нужной размерностью, и в нужном склонении
function NiceDate(chislo, type) {
    var n;
    // Узнаем нужное склонение для временной единицы
    if (chislo >= 5 && chislo <= 20)
        n = 0;
    else if (chislo == 1 || chislo % 10 == 1)
        n = 1;
    else if ((chislo <= 4 && chislo >= 1) || (chislo % 10 <= 4 && chislo % 10 >= 1))
        n = 2;
    else
        n = 0;
        

    return chislo + ' ' + LangDimensionTime[type][n];

}

// Выводит двузначное число с ведущим нулем
function ZeroPlus(x) {
    if (x < 10)
        x = '0' + x;
    return x;
}
// Переводит в 12 часовой формат
function ToAM(x) {
    if (x > 12) 
        x -= 12;
    return x;
}

// Аналог функции date() из PHP
function ParseDateFormat(format, Time) {
    var DateInFormat = '';
    if (format.length === 0)
        return;
    for (var i = 0; i < format.length; i++) {
        switch (format[i]) {
            // Часы
            // 12 часовой
            case 'g' : DateInFormat += ToAM(Time.getUTCHours()); break; // без ведущего нуля
            case 'h' : DateInFormat += ZeroPlus(ToAM(Time.getUTCHours())); break; // C ведущим нулем
            // 24 часовой
            case 'G' : DateInFormat += Time.getUTCHours(); break; // без ведущего нуля
            case 'H' : DateInFormat += ZeroPlus(Time.getUTCHours()); break; // с ведущим нулём
            // Годы
            case 'Y' : DateInFormat += Time.getUTCFullYear(); break; // Четыре цифры
            case 'y' : DateInFormat += String(Time.getUTCFullYear()).substr(2); break; // Две цифры
            // Месяцы
            case 'm' : DateInFormat += ZeroPlus(Time.getUTCMonth() + 1); break; //Порядковый номер месяца с ведущим нулём
            case 'n' : DateInFormat += Time.getUTCMonth() + 1; break; // Порядковый номер месяца без ведущего нуля
            case 'F' : DateInFormat += LangNameMonths[Time.getUTCMonth()]; break; // Полное наименование месяца
            case 'M' : DateInFormat += LangNameMonthsMin[Time.getUTCMonth()]; break; // Сокращенное наименование месяца
            // Дни
            case 'd' : DateInFormat += ZeroPlus(Time.getUTCDate()); break;// День месяца
            case 'j' : DateInFormat += Time.getUTCDate(); break; // День месяца без в.н.
            // Дни недели
            case 'N' : DateInFormat += Time.getUTCDay() + 1; break; // Порядковый номер дня недели
            case 'D' : DateInFormat += LangNameWeekdaysMin[Time.getUTCDay()]; break; // Текстовое, сокращенное, представление дня недели
            case 'L' : DateInFormat += LangNameWeekdays[Time.getUTCDay()]; break; // Полное наименование дня недели
            // Минуты
            case 'i' : DateInFormat += ZeroPlus(Time.getUTCMinutes()); break; // с ведущим нулём
            // Секунды
            case 's' : DateInFormat += ZeroPlus(Time.getUTCSeconds()); break; // с ведущим нулём
            
            default : DateInFormat += format[i]; break;
        }
    }
    
    return DateInFormat;
}

// Выводит относительное время. А так же если chek = true то просто делает проверку, относительную ли дату выводить
function OffsetDate(Time, Now, chek) {
    
    if (chek) {
        if (((new Date(Now - Time)) < (new Date(1970, 1))) || Time > Now)
            return true;
        else
            return false;
    }

    if (Time > Now)
        var OffsetTime = new Date(Time - Now);
    else
        var OffsetTime = new Date(Now - Time);
    
    var s = OffsetTime.getUTCSeconds(), // Секунды
         i = OffsetTime.getUTCMinutes(), // Минуты
         G = OffsetTime.getUTCHours(), // Часы
         j = OffsetTime.getUTCDate()-1, // Дни
         n = OffsetTime.getUTCMonth(), // Месяц
         output = '';
    
    // Если время пошло на месяцы то выводим только месяцы и дни(если не ноль)
    if (n) {
        output += NiceDate(n, 'n') + ' ';
        if (j) output += NiceDate(j, 'j') + ' ';
    // Если время пошло на дни то выводим только дни
    } else if (j) {
        output += NiceDate(j, 'j') + ' ';
    // Если время пошло на часы то выводим только часы и минуты(если не ноль)
    } else if (G) {
        output += NiceDate(G, 'G') + ' ';
        if (i) output += NiceDate(i, 'i') + ' ';
    // Если время пошло на минуты то выводим только минуты и секунды(если не ноль)
    } else if (i) {
        output += NiceDate(i, 'i') + ' ';
        if (s) output += NiceDate(s, 's') + ' ';
    // Если времени прошло менее минуты то выводим секунды
    } else
        output += NiceDate(s, 's') + ' ';
    
    if (Time > Now)
        return LangAfter + '  ' + output;
    else
        return output + '  ' + LangAgo;

}

// Выводит дату в нужном формате
function FormatTime(stime, format, el) {
    
        // Объект текущей даты
    var Now = new Date(),
        // Указанное время
        Time = new Date(stime),
        f = OffsetDate(Time, Now, true);
        
    // Выводим относительное время
    if (f)
        el.html(OffsetDate(Time, Now, false));
    else {
        // Здесь просто выводим в нужном формате...
        // Если эту дату(не относительную) мы уже обработали, то не трогаем её.
        if (!el.data('compiled')) {
            el.html(ParseDateFormat(format, Time));
            el.attr('data-compiled', 'true');
        }
    }
}

// Ищем даты на странице и изменяем их под клиента
function UpdateTime() {
    BlockTime = $('time');
    $.each(BlockTime, function () {
        el = $(this);
        format = el.data('type');
        stime = Date.parse(el.attr('datetime'));
        FormatTime(stime, format, el);
    });
}
// Первоначальная обработка времени.
$(document).ready(function(){UpdateTime();});
// Динамическое обновление дат.
setInterval(UpdateTime, 1000);







// Define the bbCode tags
packIds = [];

function checkForm() {

    formErrors = false;

    if (document.getElementById("sendForm").mainText.value.length < 2) {
        formErrors = "Вы должны ввести текст сообщения";
    }

    if (formErrors) {
        alert(formErrors);
        return false;
    }

    return true;
}

var selection = false; // Selection data


function emoticon_wospaces(text) {
    var txtarea = document.getElementById("sendForm").mainText;
    if (txtarea.createTextRange && txtarea.caretPos) {
        var caretPos = txtarea.caretPos;
        caretPos.text = caretPos.text.charAt(caretPos.text.length - 1) == ' ' ? caretPos.text + text + ' ' : caretPos.text + text;
        txtarea.focus();
    } else {
        txtarea.value  += text;
        txtarea.focus();
    }
}

// Catching selection
function catchSelection() {
    if (window.getSelection)
        selection = window.getSelection().toString();
    else if (document.getSelection)
        selection = document.getSelection();
    else if (document.selection)
        selection = document.selection.createRange().text;
}

// Putting selection to the post box
function quoteSelection(name) {
    if (selection) { 
        emoticon_wospaces('[quote="'+name+'"]' + selection + '[/quote]\n'); 
        selection = '';
        document.getElementById("sendForm").mainText.focus(); 
        return; 
    } else { 
        alert(l_no_text_selected);
        return; 
    } 
}
function in_array(value, array) {
    for(var i = 0; i < array.length; i++) {
        if(array[i] == value) return true;
    }
    return false;
}
/* add file field */
function addFileField(elementId) {
    var container = document.getElementById(elementId),
         fields = [],
         numbers = [],
         myclass = new RegExp('\\b'+'attachField'+'\\b'),
         elem = container.getElementsByTagName('*');
    
    for (var i = 0; i < elem.length; i++) {
        var classes = elem[i].className;
        if (myclass.test(classes)) {
            id = parseInt(elem[i].id.substring(6));
            numbers.push(id);
            fields.push(elem[i]);
        }
    }
    
    if (maxAttachedFiles == undefined) maxAttachedFiles = 5;
    
    var cntFields = fields.length + 1;
    if (cntFields <= maxAttachedFiles) {
        if (cntFields < 1) {
            cntFields = 1;
        }
        //Проверка, чтобы не загружать файл в одну и ту же форму и не забыть заполнить все формы.
        i = 1;
        while (in_array(i, numbers)) {
            i++;
        }

        var new_div = document.createElement('div');
        if (window.AddAttachedFormHtml != undefined)
            new_div.innerHTML = AddAttachedFormHtml.replace(/\[id\]/g, i);
        else {
            new_div.innerHTML = ' [' + i + '] ';
            new_div.innerHTML += '<input type="file" id="attach' + i + '" name="attach' + i + '" class="attachField" onChange="getFile(' + i + ')" /><span id="attachMeta' + i + '"></span>';
        }
        container.appendChild(new_div);
    }
}

/* get and identific file */
function getFile(n){
    var t = document.getElementById('attach'+n);
    if (t.value){
        ext = new Array('png','jpg','gif','jpeg','ico','bmp');
        var img = t.value.replace(/\\/g,'/');
        var pic = img.toLowerCase();
        var ok=0;
        for (i=0;i<ext.length;i++){
            m = pic.indexOf('.' + ext[i]);
            if (m != -1){
                ok=1;
                break;
            }
        }
        var d = document.getElementById('attachMeta'+n);
        if (d) {
            if (ok==1){
                var code='{IMAGE'+n+'}';
                document.getElementById('attachMeta'+n).innerHTML=' <input type="text" onmouseover="this.select();" onmouseout="this.value +=\' \'; this.value = this.value.slice(0, -1);" readonly value="'+code+'" title="Вставьте этот код в любое место сообщения" size="'+(code.length)+'" style="font-family:monospace;color:#FF8E00;cursor:move;" />';
            } else {
                document.getElementById('attachMeta'+n).innerHTML='';
            }
        }
    } else {
        document.getElementById('attach'+n).innerHTML='';
    }
} 

/**
 * Users rating
 */
function setRating(uid, formId) {
    var fpoints = $('#' + formId + ' input[name=points]:checked:first');
    if (fpoints[0] != undefined) var points = fpoints[0].value;
    else var points = 0;
    
    var fcomm = $('#' + formId + ' textarea[name=comment]:first');
    if (fcomm[0] != undefined) var comm = fcomm[0].value;
    else var comm = '';
    
    $.post('/users/rating/' + uid + '/' + points, {"points":points,"comment":comm}, function(data){
        if (data == 'ok') {
            var infomess = 'Голос добавлен';
        } else {
            var infomess = data;
        }
        $('#infomess_' + uid).html(infomess);
        setTimeout("$('#setRating_"+uid+"').hide()", 2000);
        return true;
    });
}
function addWarning_(uid, formid) {
    var str = $('#'+formid).serialize();
    $.post('/users/add_warning/'+uid, str, function(data){
        if (data == 'ok') {
            var infomess = 'Голос добавлен';
        } else {
            var infomess = data;
        }
        $('#winfomess_'+uid).html(infomess);
        setTimeout("$('#addWarning_"+uid+"').hide()", 2000);
        return true;
    });
}
/**
 * Ajax window
 */
function showFpsWin(url, params, title) {
    $.get(url, params, function(data){
        var div = document.createElement('div');
        div.innerHTML = createFpsWin(title, data, '');
        document.body.appendChild(div);
        return true;
    });
}

/**
 * Create fps window
 */
function createFpsWin(title, data, params) {
        var blid = 'setRating_' + Math.floor((Math.random()*9999));
        var fpsWin = '<div id="' + blid + '" class="fps-fwin" style="'+params+'"><div class="drag_window"><div class="fps-title" onmousedown="drag_object(event, this.parentNode)">' + title + '</div><div onClick="$(\'#' + blid + '\').hide()" class="fps-close"></div><div class="fps-cont">' + data + '</div></div></div>';
        return fpsWin;
}

/**
 * Delete user vote
 */
function deleteUserVote(voteID) {
    $.get('/users/delete_vote/' + voteID, '', function(data){
        if (data == 'ok') {
            $('#uvote_' + voteID).hide();
        }
    });
}
function deleteUserWarning(wID) {
    $.get('/users/delete_warning/' + wID, '', function(data){
        if (data == 'ok') {
            $('#uvote_w' + wID).hide();
        }
    });
}

/**
 * For Fps Windows
 */
function drag_object( evt, obj )
{
    evt = evt || window.event;
    
    // флаг, которые отвечает за то, что мы кликнули по объекту (готовность к перетаскиванию)
    obj.clicked = true;
    
    // устанавливаем первоначальные значения координат объекта
    obj.mousePosX = evt.clientX;
    obj.mousePosY = evt.clientY;

    // отключаем обработку событий по умолчанию, связанных с перемещением блока (это убирает глюки с выделением текста в других HTML-блоках, когда мы перемещаем объект)
    if( evt.preventDefault ) evt.preventDefault(); 
    else evt.returnValue = false;
    
    // когда мы отпускаем кнопку мыши, убираем «проверочный флаг»
    document.onmouseup = function(){ obj.clicked = false }
    
    // обработка координат указателя мыши и изменение позиции объекта
    document.onmousemove = function( evt )
    {
        evt = evt || window.event;
        if( obj.clicked )
        {
            posLeft = !obj.style.left ? obj.offsetLeft : parseInt( obj.style.left );
            posTop = !obj.style.top ? obj.offsetTop : parseInt( obj.style.top );

            mousePosX = evt.clientX;
            mousePosY = evt.clientY;

            obj.style.left = posLeft + mousePosX - obj.mousePosX + 'px';
            obj.style.top = posTop + mousePosY - obj.mousePosY + 'px';
            
            obj.mousePosX = mousePosX;
            obj.mousePosY = mousePosY;
        }
    }
}



/**
 * Selector for package actions
 */
function addToPackage(id) {
    packIds.push(id);
    var button = document.getElementById('packButton');
    button.value = '(' + packIds.length + ')';
    if (packIds.length > 0) button.disabled = false;
}
function delFromPackage(id) {
    for(key in packIds) {
        if(packIds[key] == id) {
            packIds.splice(key, 1);
        }
    }
    var button = document.getElementById('packButton');
    button.value = '(' + packIds.length + ')';
    if (packIds.length < 1) button.disabled = true;
}
function sendPack(action) {
    var pack = document.getElementById('actionPack');
    pack.action = action;
    for(key in packIds) {
        pack.innerHTML += '<input type="hidden" name="ids[]" value="' + packIds[key] + '">';
    }
    pack.submit();
}
function checkAll(_className, check) {
    var f = $('input.' + _className);
    for (key in f) {
        var ent = f[key];
        if (typeof ent.value != 'undefined') {
            ent.checked = check;
            if(check) addToPackage(ent.value);
            else delFromPackage(ent.value);
        }
    }
}

function check_pm(uid){
    if (uid > 0) {
        $.get('/users/get_count_new_pm/'+uid, {}, function(data){
            if (typeof data != 'undefined' && parseInt(data) == data && data > 0) {
                $('body').append(createFpsWin('Новые сообщения', '<div style="text-align:center;">' + data + ' Новых сообщений!<br><br><a href="/users/pm/">Прочитать</a></div>'));
                if(typeof available_new_pm == 'function') {
                    available_new_pm(data);
                }
            } else {
                setTimeout("check_pm("+uid+")", 20000);
            }
        });
    }
}

function setGroup(uid, formId) {
    var fgroup = $('#' + formId + ' option:selected');
    if (fgroup[0] != undefined) {
        var group = fgroup[0].value;
        $.post('/users/update_group/' + uid + '/' + group, {"group":group}, function(data){
            if (data == 'ok') {
                $('#infomess_group_' + uid).html('Группа изменена успешно!');
            } else {
                $('#infomess_group_' + uid).html(data);
            }
            return true;
        });
    }
}









/* 
 *
 * aJax окошки 2.0
 * 
 * Документация по ним тут:
 * https://bitbucket.org/atom-m/cms/wiki/Работа-с-aJax-окнами-2.0
 * 
 */

//close - показывать (true) или скрывать (false) кнопку закрытия окна. По-умолчанию true
//time - время в секундах, после которого окно закроется. По-умолчанию 0 (не закрывать)
//align - задает выравнивание контента. По-умолчанию left
//css - строка со стилями окна

// Создаёт окно
function fpsWnd(name, title, content, params) {
    if (name && name.length > 0) {
        var i = $("#"+name).length;
        if (i>0) {
            fpsWnd.content(name, content)
            fpsWnd.show(name)
            return false
        }
    }
    
    props = $.extend({
        close: true,
        time: 0,
        align: 'left',
        css: ''
    }, params || {});

    var win = '<div id="'+name+'" class="fps-fwin" style="'+props.css+'"> \
        <div class="drag_window"> \
        <div class="fps-title" onmousedown="drag_object(event, this.parentNode)">'+title+'</div>';
        
        if (props.close) {
            win += '<div onClick="$(\'#'+name+'\').hide()" class="fps-close"></div>';
        };

        win += '<div class="fps-cont" style="text-align: '+props.align+'">'+content+'</div> \
            </div> \
            </div>';
    $('body').append(win);

    if (props.time>0) {
        setTimeout(function() {
            fpsWnd.hide(name);
        }, props.time);
    };

}

// Отправляет форму на сервер и открывыет окно со статусом выполненного действия
function sendu(e, title, params) {
    if (e instanceof Object != true) {
        e = $('#'+e);
    }
    if (title == undefined) {
        title = 'Информация';
    }
    fpsWnd('fpsWinSendu', title, '<span id="loader"><img src="/sys/img/ajaxload.gif" alt="loading"></span>', params)
    setTimeout(function(){
        if ($(e).attr("action")) {
            // если форма то отправлять с помощью библиотеки
            $(e).ajaxSubmit({success: sendu_response});
        } else {
            // иначе старым способом
            jQuery.ajax({
                url:     $(e).attr("href"),
                type:     "POST",
                dataType: "html",
                data: jQuery(e).serialize(), 
                success: function(response) {
                    if ($('<div>'+response+'</div>').find("div#id_senduwin").length != 0) {
                        fpsWnd.content('fpsWinSendu', response);
                    } else {
                        fpsWnd.hide('fpsWinSendu');
                        history.pushState('', '', $(e).attr("href"));
                        var content = $('body').html();
                        $('body').html(response);
                        $(window).bind('popstate', function() {
                            history.pushState('', '', this.href);
                            $('body').html(content);
                        });
                    }
                },
                error: function(response) {
                    fpsWnd.content('fpsWinSendu', "Ошибка при отправке формы");
                }
            }); 
        };
    }, 1);
    // костыль, чтобы визуальный редактор успел отправить сформированное сообщение в textarea
}
function sendu_response(responseText, statusText, xhr, $form)  {
    fpsWnd.content('fpsWinSendu', responseText);
}

// Скрывает окно
fpsWnd.hide = function (name) {
    $("#"+name).css({ display: "none" })
};
// Показывает окно
fpsWnd.show = function (name) {
    $("#"+name).css({ display: "block" })
};
// Меняет заголовок окна
fpsWnd.header = function (name, content) {
    $('#'+name+' .fps-title').html(content);
};
// Меняет содержимое окна
fpsWnd.content = function (name, content) {
    $('#'+name+' .fps-cont').html(content);
};