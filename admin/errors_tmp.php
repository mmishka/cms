<?php
/**
* @project    Atom-M CMS
* @package    Editor templates for errors
* @url        http://cms.modos189.ru
*/

include_once '../sys/boot.php';
include_once ROOT . '/admin/inc/adm_boot.php';
$pageTitle = 'Редактирование страниц ошибок';
$pageNav = $pageTitle;
$Register = Register::getInstance();
$config = $Register['Config']->read('all');


if (isset($_GET['tmp']))
    $name = $_GET['tmp'];
else
    $name = 'default';

if (isset($_POST['template']))
    @file_put_contents(ROOT . '/data/errors/' . $name . '.html', $_POST['template']);

$template = @file_get_contents(ROOT . '/data/errors/' . $name . '.html');

include_once ROOT . '/admin/template/header.php';
?>

<form action="<?php echo $_SERVER['REQUEST_URI'];?>" method="POST">
<div class="white">
	<div class="pages-tree">
		<div class="title">Страницы</div>
		<div class="wrapper">
			<div class="tba1"><a href="errors_tmp.php?tmp=hack"><?php echo __('error template for hacker'); ?></a></div>
			<div class="tba1"><a href="errors_tmp.php?tmp=403"><?php echo __('error template for 403'); ?></a></div>
			<div class="tba1"><a href="errors_tmp.php?tmp=404"><?php echo __('error template for 404'); ?></a></div>
			<div class="tba1"><a href="errors_tmp.php?tmp=ban"><?php echo __('error template for ban'); ?></a></div>
			<div class="tba1"><a href="errors_tmp.php?tmp=default"><?php echo __('error template for default'); ?></a></div>
		</div>
		<div style="width:100%;">&nbsp;</div>
	</div>
	<div class="list pages-form">
		<div class="title">Редактор шаблонов ошибок</div>
		
		<div class="level1">
			<div class="items">
				<div class="setting-item">
					<div class="center">
						<textarea style="width:99%;height:380px;" wrap="off" name="template" id="tmpl"><?php print h($template); ?></textarea>
					</div>
					<div class="clear"></div>
				</div>
				<div class="setting-item">
					<div class="left"></div>
					<div class="right">
						<input class="save-button" type="submit" name="send" value="Сохранить" />
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>
</form>

<script type="text/javascript" src="js/codemirror/codemirror.js"></script>
<script type="text/javascript" src="js/codemirror/mode/javascript/javascript.js"></script>
<script type="text/javascript" src="js/codemirror/mode/xml/xml.js"></script>
<script type="text/javascript" src="js/codemirror/mode/htmlmixed/htmlmixed.js"></script>
<script type="text/javascript" src="js/codemirror/mode/css/css.js"></script>

<link rel="StyleSheet" type="text/css" href="js/codemirror/codemirror.css" />
<script type="text/javascript">
$(document).ready(function(){
    var editor = CodeMirror.fromTextArea(document.getElementById("tmpl"), {
		mode: "text/html"
	});
	editor.setSize('100%', 450);
});
</script>

<ul class="markers">
	<h2>Метки для использования на страницах ошибок</h2>
	<li><div class="global-marks">{{ error.site_domain }}</div> - Домен вашего сайта.</li>
	<li><div class="global-marks">{{ error.site_title }}</div> - Название вашего сайта.</li>
	<li><div class="global-marks">{{ error.code }}</div> - Код ошибки</li>
</ul>

<?php include_once 'template/footer.php'; ?>