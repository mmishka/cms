<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.5, user-scalable=no">
	<title><?php echo $pageTitle; ?></title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<script language="JavaScript" type="text/javascript" src="../data/js/jquery.js"></script>
	<script type="text/javascript" src="js/drunya.lib.js"></script>
	<link rel="StyleSheet" type="text/css" href="template/css/style.css" />

	<script type="text/javascript">

	$(document).ready(function(){
		setTimeout(function(){
			$('#overlay').height($('#wrapper').height());
		}, 2000);
	});
	</script>

	<link type="text/css" rel="StyleSheet" href="template/css/fancy.css" />
	<script type="text/javascript" src="js/jquery.fancybox.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		$("a.gallery").fancybox();
	});
</script>
</head> 
<body>
<span id="linkleftmenu" onclick="leftm()"></span>
<span id="linkrightmenu" onclick="rightm()"></span>
	<div id="wrapper">
	    <!--menus-->
        <div class="rcrumbs">
            <?php echo (!empty($pageNavr)) ? $pageNavr : ''; ?>
        </div>
        <div class="crumbs">
            <div><?php echo (!empty($pageNav)) ? $pageNav : ''; ?></div>
        </div>
		<div class="side-menu" id="leftmenu">
							<!--<div class="search">
								<form>
									<div class="input"><input type="text" name="search" placeholder="Search..." /></div>
									<input class="submit-butt" type="submit" name="send" value="" />
								</form>
							</div>-->
							<div class="nameleftmenu">Модули</div>
							<ul>
							
							
							
							<?php
							$modsInstal = new FpsModuleInstaller;
							$nsmods = $modsInstal->checkNewModules();

							if (count($nsmods)):
								foreach ($nsmods as $mk => $mv):
							?>	
							
								<li>
									<div class="icon new-module"></div><a href="#"><?php echo $mk; ?></a>
									<div class="sub-opener" onClick="subMenu('sub<?php echo $mk; ?>')"></div>
									<div class="clear"></div>
									<div id="sub<?php echo $mk; ?>" class="sub">
										<div class="shadow">
											<ul>
												<li><a href="<?php echo WWW_ROOT; ?>/admin?install=<?php echo $mk ?>">Install</a></li>
											</ul>
										</div>
									</div>
								</li>
							<?php
								endforeach;
							endif;




                            $modules = getAdmFrontMenuParams();

                            $i=0;
                            foreach ($modules as $modKey => $modData): 
                                if (!empty($nsmods) && array_key_exists($modKey, $nsmods)) continue;
                                if (Config::read('active', $modKey) != 1) continue;
                                if ($i==0) {
                                ?>
                                    <li class="cat_on">
                                        Включенные модули
                                    </li>
                                <?php
                                }
                                $i=1;
                                ?>
                                <li>
                                    <div class="icon <?php echo $modKey ?>"></div><a href="<?php echo $modData['url']; ?>"><?php echo $modData['ankor']; ?></a>
                                    <div class="sub-opener" onClick="subMenu('sub<?php echo $modKey ?>')"></div>
                                    <div class="clear"></div>
                                    <div id="sub<?php echo $modKey ?>" class="sub">
                                        <div class="shadow">
                                            <ul>
                                                <?php foreach ($modData['sub'] as $url => $ankor): ?>
                                                <li><a href="<?php echo $url; ?>"><?php echo $ankor; ?></a></li>
                                                <?php endforeach; ?>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            <?php endforeach;

                            $i=0;
                            foreach ($modules as $modKey => $modData):
                                if (!empty($nsmods) && array_key_exists($modKey, $nsmods)) continue;
                                if (Config::read('active', $modKey) == 1) continue;
                                if ($i==0) {
                                ?>
                                    <li class="cat_off">
                                        Выключенные модули
                                    </li>
                                <?php
                                }
                                $i=1;
                                ?>
                                <li>
                                    <div class="icon <?php echo $modKey ?>"></div><a href="<?php echo $modData['url']; ?>"><?php echo $modData['ankor']; ?></a>
                                    <div class="sub-opener" onClick="subMenu('sub<?php echo $modKey ?>')"></div>
                                    <div class="clear"></div>
                                    <div id="sub<?php echo $modKey ?>" class="sub">
                                        <div class="shadow">
                                            <ul>
                                                <?php foreach ($modData['sub'] as $url => $ankor): ?>
                                                <li><a href="<?php echo $url; ?>"><?php echo $ankor; ?></a></li>
                                                <?php endforeach; ?>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            <?php endforeach; ?>

                            </ul>
                            <div class="clear"></div>
					</div>
		
		<!-- /AdminBar -->
		<div class="headmenu" id="rightmenu">
		    <span class="homelink"><a href="/admin" title="<?php echo __('Main page'); ?>"></a></span>
			<div class="menu" id="topmenu">
				<ul>
					<li><span>Общее</span></li>
					<li><span>Плагины</span></li>
					<li><span>Сниппеты</span></li>
					<li><span>Дизайн</span></li>
					<li><span>Статистика</span></li>
					<li><span>Безопасность</span></li>
					<li><span>Дополнительно</span></li>
					<li><span>Помощь</span></li>
					<div class="clear"></div>
				</ul>
			</div>
			<div class="userbar">
				<?php
				if (!empty($_SESSION['user']['name'])) {
					$ava_path = getAvatar($_SESSION['user']['id'], $_SESSION['user']['email']);
					$user_url = get_url('users/info/' . $_SESSION['user']['id']);
				}
				@ini_set('default_socket_timeout', 5);
				$new_ver = @file_get_contents('http://cms.modos189.ru/last.php?host=' . $_SERVER['HTTP_HOST']);
				$new_ver = ((!empty($new_ver)) && ($new_ver == h($new_ver)) && ($new_ver != FPS_VERSION)) 
				? '<a class="usually_link" href="https://bitbucket.org/atom-m/cms" title="Last version">' . h($new_ver) . '</a>' 
				: '';
				?>
				<div class="ava"><img src="<?php echo $ava_path; ?>" alt="user ava" title="user ava" /></div>
				<div class="name"><a href="<?php echo $user_url; ?>" target="_blank"><?php echo h($_SESSION['user']['name']); ?></a><span>Admin</span></div>
				<a href="exit.php" class="exit" title="Выход"></a>
			</div>
			<div class="clear"></div>
		</div>
	    <script type="text/javascript">

		document.top_menu = new drunyaMenu([
		['<?php echo __('General'); ?>',
		  [
		  '<span><?php echo __('Version of Atom-M'); ?><br /> [ <b><?php echo FPS_VERSION ?></b> ]<?php if ($new_ver): ?><br /><?php echo __('New version of Atom-M'); ?><br /> [ <?php echo $new_ver; ?> ]<?php endif; ?></span>',
		  '<a href="/admin/settings.php?m=sys"><?php echo __('Common settings'); ?></a>',
		  '<a href="/admin/clean_cache.php"><?php echo __('Clean cache'); ?></a>'
		  ]],

		['<?php echo __('Plugins'); ?>',
		  [
		  '<a href="/admin/plugins.php" style="text-align: center"><?php echo 'Менеджер плагинов'; ?></a>',
		  <?php
            $Cache = new Cache;
            $Cache->lifeTime = 6000;
            if ($Cache->check('adm_pl_settings')) {
                $list = $Cache->read('adm_pl_settings');
            } else {
                $plugins = glob(ROOT . '/plugins/*');
                $list = '';
                if (!empty($plugins) && count($plugins) > 0) {
                    foreach ($plugins as $pl) {
                        if (file_exists($pl . '/config.json')) {
                            $config = json_decode(file_get_contents($pl . '/config.json'), 1);
                            if (!empty($config['active'])) {
                                $settigs_file_path = $pl . '/settings.php';
                                if (file_exists($settigs_file_path))
                                    $list .= "'<a href=\"/admin/plugins.php?ac=edit&dir=".substr($pl, strripos($pl, '/')+1)."\">".h($config['title'])."</a>',";
                            }
                        }
                    }
                }
                $Cache->write($list, 'adm_pl_settings', array());
            }
            echo $list;
		  ?>
		  ]],

		  
		  
		['<?php echo __('Snippets'); ?>',
		  [
		  '<a href="/admin/snippets.php"><?php echo __('Create'); ?></a>',
		  '<a href="/admin/snippets.php?a=ed"><?php echo __('Edit'); ?></a>'
		  ]],

		  
		 
		['<?php echo __('Design'); ?>',
		  [
		  '<a href="design.php?d=default&t=main"><?php echo __('General design and css'); ?></a>',
		  '<a href="menu_editor.php"><?php echo __('Menu editor'); ?></a>',
          '<a href="errors_tmp.php"><?php echo __('Error pages'); ?></a>'
		  ]],


		['<?php echo __('Security'); ?>',
		  [
		  '<a href="settings.php?m=secure"><?php echo __('Security settings'); ?></a>',
		  '<a href="system_log.php"><?php echo __('Action log'); ?></a>',
		  '<a href="ip_ban.php"><?php echo __('Bans by IP'); ?></a>',
		  '<a href="dump.php"><?php echo __('Backup control'); ?></a>'
		  ]],
		  
		  
		['<?php echo __('Additional'); ?>',
		  [
		  '<a href="settings.php?m=seo"><?php echo __('SEO settings'); ?></a>',
		  '<a href="settings.php?m=rss"><?php echo __('RSS settings'); ?></a>',
		  '<a href="settings.php?m=sitemap"><?php echo __('Sitemap settings'); ?></a>',
		  '<a href="settings.php?m=preview"><?php echo __('Preview settings'); ?></a>',
		  '<a href="settings.php?m=watermark"><?php echo __('Watermark settings'); ?></a>'
		  ]],
		  

		['<?php echo __('Help'); ?>',
		  [
		  '<a href="http://atomx.net" target="_blank"><?php echo __('AtomX CMS Community'); ?></a>',
		  '<a href="faq.php"><?php echo __('FAQ'); ?></a>',
		  '<a href="authors.php"><?php echo __('Dev. Team'); ?></a>',
		  ]]
		]);

		</script>
		<!-- /AdminBar -->
		<!--/menus-->
		
		<div class="center-wrapper">
		
			<div class="side-separator">
				<div style="position:relative;">

						<div id="content-wrapper">






<?php /*
<!-- navi -->
<div class="topnav">
<div class="left"><?php echo $pageNav; ?><div class="right"><?php echo $pageNavl; ?></div></div>
</div>
<!-- /navi -->
*/ ?>










