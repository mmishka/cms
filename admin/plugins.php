<?php
/**
* @project    Atom-M CMS
* @package    Plugins manager
* @url        http://cms.modos189.ru
*/

include_once '../sys/boot.php';
include_once './inc/adm_boot.php';

$Register = Register::getInstance();

if (!isset($_GET['ac'])) $_GET['ac'] = 'index';
$actions = array('index', 'list', 'install', 'on', 'off', 'edit', 'local');

switch ($_GET['ac']) {
    case 'index':
        $content = index();
        break;
    case 'list':
        $content = pl_list($_GET['type']);
        break;
    case 'install':
        $content = pl_url($_GET['url'],$_GET['dir']);
        break;
    case 'on':
        $content = switchPlugin(1, $_GET['dir']);
        break;
    case 'off':
        $content = switchPlugin(0, $_GET['dir']);
        break;
    case 'edit':
        $content = editPlugin($_GET['dir']);
        break;
    case 'local':
        $content = local();
        break;
    default:
        $content = index();
}
echo $content;

// главная страница менеджера
function index() {
    $pageTitle = 'Менеджер плагинов';
    $pageNav = $pageTitle;
    $pageNavr = '<a href="#" onclick="openPopup(\'sec\');">Установить из архива</a>';
    include_once ROOT . '/admin/template/header.php';
    $content = '

        <style>
            @import url(http://fonts.googleapis.com/css?family=Roboto&subset=latin,cyrillic);
            #main {
                min-height: 60px;
            }

            #chswitch {
                margin-top: 10px;
                text-align: center;
            }
            #chswitch div {
                padding: 10px;
                display: inline-block;
                cursor: pointer;
                margin: 1px;
                border-bottom: 2px solid #d0d0d0;
            }
            #chswitch div:hover, #chswitch .act {
                border-bottom: 2px solid #ff7e00;
            }

            #tiles li {
                background: #F8F8F8;
                border: 1px solid #C6C6C6;
                box-shadow: 0 1px 3px #EEEEEE;
                width: 220px;
                min-height: 220px;
                list-style: none outside none;
                padding: 5px;
                position: absolute;
                left: -1000px;
            }
            #tiles li:hover {
                background-color: #FEFEFE;
            }
            #tiles li .title {
                font: 300 14px \'Roboto\',arial,sans-serif;
                margin-top: 0;
                margin-bottom: 5px;
                text-align: center;
            }
            #tiles li img {
                width: 220px;
                max-height: 220px;
            }
            #tiles li .desc {
                color: #444;
            }
            #tiles li.on {
                background-color: #EAF1D3;
            }
            #tiles li.off {
                background-color: #FFDDDD;
            }

            .buttoms {
                text-align:right;
                font-family: \'icomoon\';
                font-size: 20px;
            }
            .buttoms a {
                width: 22px;
                height: 22px;
                text-decoration: none;
                color: #777;
                transition: .3s ease-in-out;
                padding: 5px;
            }
            .buttoms a:hover {
                color:#333;
            }
            .buttoms a.link {
                float: left;
            }
            .buttoms a.edit, .buttoms a.on, .buttoms a.off, .buttoms a.install {
                float: right;
            }
            .buttoms a.edit:before {
                content: "\e08e";
            }
            .buttoms a.on:before {
                content: "\e267";
            }
            .buttoms a.off:before {
                content: "\e269";
            }
            .buttoms a.link:before {
                content: "\e0c3";
            }
            .buttoms a.install:before {
                content: "\e0bd";
            }

            .popup .items ul li {
                border-left: 1px solid #DDDDDD;
                border-right: 1px solid #DDDDDD;
                border-top: 1px solid #DDDDDD;
                letter-spacing: 1px;
                list-style-type: none;
                padding: 5px;
            }
            .popup .items ul li:nth-child(2n) {
                background: none repeat scroll 0 0 #F9F9F9;
            }
            .popup .items ul li:last-child {
                border-bottom: 1px solid #DDDDDD;
                border-radius: 0 0 4px 4px;
            }
            .popup .items ul li:first-child {
                border-radius: 4px 4px 0 0;
            }
            .popup .items ul li:hover {
                background: none repeat scroll 0 0 #EEEEEE;
            }
            .popup .items ul li a:hover {
                text-decoration: none;
            }
        </style>

        <div id="sec" class="popup">
            <div class="top">
                <div class="title">Установить из архива (формат ZIP, не более 10МиБ)</div>
                <div onClick="closePopup(\'sec\');" class="close"></div>
            </div>
            <form id="locsend" method="post" action="plugins.php?ac=local" onSubmit="locsend(this); return false" enctype="multipart/form-data">
            <div class="items">
                <div class="clear">&nbsp;</div>
                <div class="item">
                    <div class="left">
                        URL:
                        <span class="comment">Загрузить плагин с удаленного сервера</span>
                    </div>
                    <div class="right"><input type="text" name="pl_url" placeholder="http://site.com/path/to/plugin.zip" /></div>
                    <div class="clear"></div>
                </div>
                <div class="clear">&nbsp;</div>
                <div class="item">
                    <div class="left">
                        Загрузить файл:
                        <span class="comment">Загрузить плагин как локальный файл</span>
                    </div>
                    <div class="right">
                        <input type="file" accept="application/zip" name="pl_file" onChange="if (pl_file.value.substring(pl_file.value.lastIndexOf(\'.\')+1, pl_file.value.length).toLowerCase() != \'zip\')
                            { alert(\'File type should be ZIP\'); return; };" />
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear">&nbsp;</div>
                <div class="item submit" style="padding-top: 0; padding-bottom: 30px">
                    <div class="left"></div>
                    <div style="float:left;" class="right">
                        <input type="submit" value="Установка" name="send" class="save-button">
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
            </form>
        </div>

        <div id="popup" class="popup">
            <div class="top">
                <div class="title">Установка</div>
                <div onClick="closePopup(\'popup\');" class="close"></div>
            </div>
            <div id="po_cont">
            </div>
        </div>

        <div id="chswitch"><div class="catalog">Каталог</div><div class="installed">Установленные</div></div>
        <div id="main" style="position:relative; padding-bottom: 100px">
            <ul id="tiles">
            </ul>
        </div>

        <script src="/admin/js/jquery.wookmark.min.js"></script>
        <script src="/admin/js/jquery.imagesloaded.js"></script>
        <script>
            function draw() {
                handler = $(\'#tiles li\');
                options = {
                    align: \'center\',
                    autoResize: true,
                    container: $(\'#main\'),
                    offset: 7,
                    outerOffset: 10,
                    itemWidth: 232
                };
                if (handler.wookmarkInstance) {
                    handler.wookmarkInstance.clear();
                }
                handler.wookmark(options);
                wind(); // Перерасчет размеров окна для нормального отображения меню, в т.ч. и в мобильной версии.
            }
            function load() {
                type = \'catalog\';
                if ($(\'#chswitch .installed\').hasClass(\'act\')) {
                    type = \'installed\';
                }
                $.ajax({
                    url: "/admin/plugins.php",
                    data: "ac=list&type="+type,
                    cache: false,
                    success: function(html){
                        $("#tiles").html(html);
                        draw();
                        setInterval(draw, 500);
                    }
                });
            }

            function clear() {
                $(\'#chswitch .catalog\').removeClass(\'act\');
                $(\'#chswitch .installed\').removeClass(\'act\');
            }
            $(\'#chswitch .catalog\').click(function() {
                clear();
                $(\'#chswitch .catalog\').addClass(\'act\');
                load();
            });
            $(\'#chswitch .installed\').click(function() {
                clear();
                $(\'#chswitch .installed\').addClass(\'act\');
                load();
            });

            $(\'#chswitch .catalog\').click();

            function openPopupNew(e) {
                resizeWrapper($(\'#popup\'));
                $(\'#popup\').fadeIn(1000);
                if (!$(\'div#overlay\').is(\':visible\')) {
                    $(\'div#overlay\').fadeIn();
                }
                $(\'#po_cont\').html("<div class=\"items\">Пожалуйста, подождите...</div>");
                jQuery.ajax({
                    url:     $(e).attr("href"),
                    type:     "POST",
                    dataType: "html",
                    data: jQuery(e).serialize(), 
                    success: function(response) {
                        $(\'#po_cont .items\').html(response);
                        load();
                    },
                    error: function(response) {
                        $(\'#po_cont .items\').html("Ошибка при отправке формы");
                    }
                }); 
            }

            function asend(e) {
                jQuery.ajax({
                    url:     $(e).attr("href"),
                    type:     "POST",
                    dataType: "html",
                    data: jQuery(e).serialize(), 
                    success: function(response) {
                        load();
                    },
                    error: function(response) {
                        resizeWrapper($(\'#popup\'));
                        $(\'#popup\').fadeIn(1000);
                        if (!$(\'div#overlay\').is(\':visible\')) {
                            $(\'div#overlay\').fadeIn();
                        }
                        $(\'#po_cont\').html("<div class=\"items\">Ошибка при отправке формы</div>");
                    }
                }); 
            }

            function locsend(e) {
                closePopup(\'sec\');
                resizeWrapper($(\'#popup\'));
                $(\'#popup\').fadeIn(1000);
                if (!$(\'div#overlay\').is(\':visible\')) {
                    $(\'div#overlay\').fadeIn();
                }
                $(\'#po_cont\').html("<div class=\"items\">Пожалуйста, подождите...</div>");

                form = document.forms.locsend;
                formData = new FormData(form);
                xhr = new XMLHttpRequest();
                xhr.open("POST", $(e).attr("action"));

                xhr.onreadystatechange = function() {
                    if (xhr.readyState == 4) {
                            data = xhr.responseText;
                            $(\'#po_cont .items\').html(data);
                            load();
                    }
                };
                xhr.send(formData);

            }


        </script>
    ';
    //$(window).bind(\'scroll.wookmark\', onScroll);

    echo $content;
    include_once ROOT . '/admin/template/footer.php';
    return '';
}


// показ списка плагинов
// $type - выбор вкладки для отображения
function pl_list($type) {
    $content = '';

    if ($type == 'catalog') {
        $Cache = new Cache;
        $Cache->lifeTime = 600;
        if ($Cache->check('adm_pl_list')) {
            $source_list_raw = $Cache->read('adm_pl_list');
            $source_list = json_decode($source_list_raw, true);
            $plugins = $source_list['plugins'];
        } else {
            $source_url = 'http://borislap.ru/api/plugins.json';
            $source_list_raw = file_get_contents($source_url);
            $source_list = json_decode($source_list_raw, true);
            $plugins = $source_list['plugins'];
            if (count($plugins) < 1)
                return $content .= '<div class="warning">Каталог плагинов недоступен</div>';
            else
                $Cache->write($source_list_raw, 'adm_pl_list', array());
        }
    } elseif ($type == 'installed') {
        $plugins = glob(ROOT . '/plugins/*');
        if (!empty($plugins) && count($plugins) > 0) {
            foreach ($plugins as $k => $pl) {
                if (!is_dir($pl)) unset($plugins[$k]);
            }
        }
    } else {
        return;
    }


    foreach ($plugins as $result) {
        if ($type == 'installed') {
            $local_path = $result;
            $dir = substr($result, strripos($result, '/')+1);
        } else {
            $local_path = R . 'plugins/' . $result['name'];
            $dir = $result['name'];
        }

        $buttoms = '<div class="buttoms">';
        $class = '';
        $icon = '';
        if (file_exists($local_path . '/config.json')) {
            $result = json_decode(file_get_contents($local_path . '/config.json'), 1);
            if (!empty($result['more']))
                $buttoms .= '<a class="link" href="'.h($result['more']).'"></a>';
            if (!empty($result['active'])) {
                $settigs_file_path = R . 'plugins/' . $dir . '/settings.php';
                if (file_exists($settigs_file_path))
                    $buttoms .= '<a class="edit" title="Редактировать" href="plugins.php?ac=edit&dir='.$dir.'"></a>';
                $buttoms .= '<a class="off" title="Выключить" href="plugins.php?ac=off&dir='.$dir.'" onclick="asend(this);return false"></a>';
                $class = 'on';
            } else {
                $buttoms .= '<a class="on" title="Включить" href="plugins.php?ac=on&dir='.$dir.'" onclick="asend(this);return false"></a>';
                $class = 'off';
            }
            if (!empty($result['icon'])) {
                $icon_data = base64_encode(file_get_contents($local_path.'/'.$result['icon']));
                $size = getimagesize($local_path.'/'.$result['icon']);
                $icon = "<img src=\"data: ".$size['mime'].";base64,".$icon_data."\">";
            }
        } else {
            if ($type == 'installed') {
                continue;
            } else {
                $path_file = sprintf($source_list['path_file'], $result['name']);
                if (!empty($result['more']))
                    $buttoms .= '<a class="link" href="'.h($result['more']).'"></a>';
                $buttoms .= '<a href="plugins.php?ac=install&url='.$path_file.'&dir='.$dir.'" class="install" title="Установить" onclick="openPopupNew(this);return false"></a>';
                if (!empty($result['icon'])) {
                    $path_icon = sprintf($source_list['path_icon'], $result['name']).$result['icon'];
                    $icon = "<img src=\"".h($path_icon)."\">";
                }
            }
        }
        $buttoms .= '</div>';

        $content .= '<li class="'.$class.'">';
        $content .= '<div style="min-height: 188px">';
        $content .= "<p class=\"title\">".h($result['title'])."</p>";
        $content .= $icon;
        $content .= "<p class=\"desc\">".h((empty($result['desc']) ? 'Описание не предусмотрено.' : $result['desc'])).'</p>';
        $content .= '</div>';
        $content .= $buttoms;

        $content .= '</li>';
    }

    return $content;
}


// вывод ошибки
function showError() {
    $Register = Register::getInstance();
    $errors = $Register['PluginController']->getErrors();
    return $errors;
}


// включение и выключение плагина
// $to - boolear, $dir - название папки плагина
function switchPlugin($to, $dir) {
    $to = (int)(bool)$to;
    if (empty($dir)) redirect('/');
    $pach = ROOT . '/plugins/' . $dir;
    $conf_pach = $pach . '/config.json';

    $config = (file_exists($conf_pach)) ? json_decode(file_get_contents($conf_pach), 1) : array();
    $config['active'] = $to;
    file_put_contents($conf_pach, json_encode($config));

    $Cache = new Cache;
    $Cache->remove('adm_pl_settings');

    redirect('../admin/plugins.php');
}


// вывод настроек плагина
// $dir - название папки плагина
function editPlugin($dir) {
    if (empty($dir)) redirect('/admin/plugins.php');
    if (!preg_match('#^[\w\d_-]+$#i', $dir)) redirect('/admin/plugins.php');

    $settigs_file_path = ROOT . '/plugins/' . $dir . '/settings.php';
    if (!file_exists($settigs_file_path)) return '<h2>No settings for this plugin</h2>';

    $pageTitle = 'Управление плагином';
    $pageNav = '<a href="/admin/plugins.php">Менеджер плагинов</a>';
    $pageNavr = '<a href="#" onclick="openPopup(\'sec\');">Установить из архива</a>';
    include_once ROOT . '/admin/template/header.php';
    include_once $settigs_file_path;
    print (!empty($output)) ? $output : '';
    include_once ROOT . '/admin/template/footer.php';
    return '';
}


// установка плагина из загруженного архива
function local() {
    // local plugin archive
    if (!empty($_FILES['pl_file']['name'])) {
        $Register = Register::getInstance();

        // download plugin to tmp folder
        $filename = $Register['PluginController']->localUpload('pl_file');
        if (!$filename) {
            showError();
        }
        echo install($filename);
    } else if (!empty($_POST['pl_url'])) {
        echo pl_url($_POST['pl_url']);
    }
}


// установка по url адресу
function pl_url($url, $dir) {
    $Register = Register::getInstance();

    // download plugin to tmp folder
    $filename = $Register['PluginController']->foreignUpload($url);
    if (!$filename) {
        return showError();
    }

    return install($filename, $dir);
}


// установка и включение плагина
// $filename - файл в /sys/tmp/
function install($filename, $dir = False) {
    $Register = Register::getInstance();

    // install plugin
    $result = $Register['PluginController']->install($filename, $dir);
    if (!$result) {
        return showError();
    }

    $files = $Register['PluginController']->getFiles();
    $message = '<h2>Плагин установлен</h2>';
    $message .= '<strong>Новые файлы:</strong><ul class="wps-list">';
    foreach ($files as $file) {
        $message .= '<li>' . $file . '</li>';
    }
    $message .= '</ul>';

    $Cache = new Cache;
    $Cache->remove('adm_pl_settings');

    return $message;
}

?>