<?php
/**
* @project    Atom-M CMS
* @package    Clean cache
* @url        http://cms.modos189.ru
*/

include_once '../sys/boot.php';
include_once ROOT . '/admin/inc/adm_boot.php';

// Keep it simple, stupid!
_unlink(ROOT . '/sys/cache/');

$meta_file = ROOT . '/sys/tmp/search/meta.dat';
if (file_exists($meta_file)) unlink($meta_file);

$_SESSION['clean_cache'] = true;

redirect('/admin');