<?php

$output = '';
$template_patch = dirname(__FILE__).'/templates/popmat.html';
$conf_pach = dirname(__FILE__).'/config.json';
$config = json_decode(file_get_contents($conf_pach), true);

if (isset($_POST['send'])) {
    $config['limit'] = $_POST['limit'];
    $config['short_main'] = $_POST['short_main'];
    $config['short_title'] = $_POST['short_title'];
    $config['module'] = $_POST['module'];
	$config['sort'] = $_POST['sort'];
    file_put_contents($conf_pach, json_encode($config));
    $f = fopen($template_patch, "w");
    fwrite($f,$_POST['template']);

    $Cache = new Cache;
    $Cache->remove('pl_popmat');

    $output .= '<div class="warning">Изменения приняты!</div>';
}


$template = file_get_contents($template_patch);

if($config['module']=='loads')
{
$downloads =  '<option value="downloads"' . (($config['sort']=='downloads') ? ' selected' : '') . '>По загрузкам</option>';
}

$output .= '
<style>
                .ib {
                    font-weight: bold;
                    font-style: italic;
                }
                .right {
                    width: 50%;
                }
                input[type="text"] {
                    height: auto;
                }
                .markers input {
                    background: #D9D9D9;
                    border-radius: 5px;
                    display: inline-block;
                    font-weight: 700;
                    padding: 5px;border:none;width:130px;text-align:center;
                }
                .mmand {
                    color:#000;
                    font-weight:bold;
                    text-align:center;
                    padding-top:4px;
                }
            </style>';

$output .= '<form action="" method="post">
                <div class="list">
                    <div class="title">Управление плагином "Популярные материалы"</div>
                    <div class="level1">

                            <div class="items">
                                <div class="setting-item">
                                    <div class="title" style="padding: 20px; text-align:left">Метка для вывода топа материалов {{ popmat }}</div>
                                </div>
                            </div>

                            <div class="items">
                                <div class="setting-item">
                                    <div class="left">Количество материалов:</div>
                                    <div class="right">
                                        <input type="text" size="100" name="limit" value="' . $config['limit'] . '">
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>

                            <div class="items">
                                <div class="setting-item">
                                    <div class="left">Ограничение заголовка</div>
                                    <div class="right">

                                        <input type="text" size="100" name="short_title" value="' . $config['short_title'] . '">
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>

                            <div class="items">
                                <div class="setting-item">
                                    <div class="left">Ограничение описания</div>
                                    <div class="right">
                                        <input type="text" size="100" name="short_main" value="' . $config['short_main'] . '">
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                         <div class="items">
                                <div class="setting-item">
                                    <div class="left">Модуль <div style="font-size:10px"> Если вы выберите модуль "Файлы", то откроется сортировка по загрузкам:)</div></div>
                                    <div class="right">
                                        <select name="module">
                                            <option value="news"' . (($config['module']=='news') ? 'selected' : '') . '>Новости</option>
                                            <option value="loads"' . (($config['module']=='loads') ? 'selected' : '') . '>Файлы</option>
                                            <option value="stat"' . (($config['module']=='stat') ? 'selected' : '') . '>Статьи</option>
                                        </select>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                </div>
								<div class="items">
                                <div class="setting-item">
                                    <div class="left">Тип сортировки </div>
                                    <div class="right">
                                        <select name="sort">
										   <option value="date"' . (($config['sort']=='date') ? ' selected' : '') . '>По дате </option>
                                            <option value="views"' . (($config['sort']=='views') ? ' selected' : '') . '>По просмотрам </option>
                                            <option value="comments"' . (($config['sort']=='comments') ? ' selected' : '') . '>По комментариям</option>
											'. $downloads .'
                                                                              
										 
                                        </select>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                </div>

                            <div class="items">
                                <div class="setting-item">
                                    <textarea name="template" style="width: 99%; height: 350px">' . (isset($template) ? h($template) : '') . '</textarea>
                                </div>
                            </div>

                            <div class="items">
                                <div class="setting-item">
                                    <div class="title" style="padding: 20px; text-align:center; height: auto;"><input name="send" type="submit" value="Сохранить" class="save-button"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>';

$output .= '<ul class="markers">
<div style="color:#ff0000">В плагине работают все поля которые есть в базе данных news. <br/>
Пример: {{ mat.названиеполя }}. <br /><b>Для новичков</b> какие поля есть: id; title; main; views; date; category_id; author_id; comments и т.д
</div>
                <li><input onclick="select(this)" value="{{ mat.img_url }}" readonly="" /> - Метка вывода адреса прикрепления</li>
                <li><input onclick="select(this)" value="{{ mat.author_name }}" readonly="" /> - Ник автора материала</li>
                <li><input onclick="select(this)" value="{{ mat.author_url }}" readonly="" /> - Ссылка на профиль автора</li>
                <li><input onclick="select(this)" value="{{ mat.url }}" readonly="" /> - Адрес материала</li>
                <li><input onclick="select(this)" value="{{ mat.title }}" readonly="" /> - Заголовок</li>
                <li><input onclick="select(this)" value="{{ mat.main }}" readonly="" /> - Описание</li>
            </ul>
            <div class="mmand">Специально для Atom-M CMS</div>
            ';

?>