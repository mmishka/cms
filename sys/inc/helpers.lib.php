<?php
/**
* @project    Atom-M CMS
* @package    Helpers library
* @url        http://cms.modos189.ru
*/


/**
 * Get one or couple entities.
 * If get one entity of the UsersModel, we also get user statistic
 *
 * @param $modelName
 * @param array $id
 * @return array
 * @throws Exception
 */
function fetch($modelName, $id = array()){
    $Register = Register::getInstance();
    try {
        $model = $Register['ModManager']->getModelInstance($modelName);

        // get collection of entities
        if (is_array($id) && count($id)) {

            $id = array_map(function($n){
                $n = intval($n);
                if ($n < 1) throw new Exception('Only integer value might send as ID.');
                return $n;
            }, $id);
            $ids = implode(", ", $id);
            $result = $model->getCollection(array("`id` IN ($ids)"));

        // get one entity
        } else if (is_numeric($id)) {
            $id = intval($id);
            if ($id < 1) throw new Exception('Only integer value might send as ID.');
            $result = $model->getById($id);

            if ($result && strtolower($modelName) == 'users') {
                $stat = $model->getFullUserStatistic($id);
                $result->setStatistic($stat);
            }
        }

    } catch (Exception $e) {
        throw new Exception($e->getMessage());
    }

   return (!empty($result)) ? $result : array();
}

// Аналог range() но возвращает пригодный для for массив. Эта функция используется для перебора по индексу в шаблонизаторе.
function a($n, $k = null, $step = 1) {
    if ($k == null) {
        $k = $n;
        $n = 0;
    }
    
    return array_slice(range($n, $k, $step), 0);
}

// Выводит текущую дату в формате, выбранном в админке. А так же помещает её в тег, который динамически обновляется под клиента.
function AtmDate($date, $format = false) {
	if (!$format) $format = Config::read('date_format');
    
    if ($date == '0000-00-00 00:00:00')
        return __('never');
        
	return '<time datetime="' . date('c', strtotime($date)) . '" data-type="' . $format . '">' . date($format, strtotime($date)) . '</time>';
}
// Выводит относительное время(которое еще не настало), по типу текущее время настало "через 20 мин." после времени $time 
// Используется для вывода временной метки при группировке постов/комментариев.
function AtmOffsetDate($time) {
    
    $time = time() - $time;
    
    $formattime = '';
    // Если разница более года
    if ($time > 31556926)
        return __('after one year');
    // Если время пошло на месяцы
    else if (date('n', $time) - 1) {
        $formattime .= date('n', $time) . ' ' . __('month.') . ' ';
    // Если время пошло на дни то выводим только дни
    } else if (date('j', $time) - 1) {
        $formattime .= date('j', $time) . ' ' . __('day.') . ' ';
    // Если время пошло на часы то выводим только часы и минуты(если не ноль)
    } else if (round($time / 3600)) {
        $formattime .= round($time / 3600) . ' ' . __('hour.') . ' ';
    // Если время пошло на минуты то выводим только минуты и секунды(если не ноль)
    } else if (round($time / 60)) {
        $formattime .= round($time / 60) . ' ' . __('minute.') . ' ';
    // Если времени прошло менее минуты то выводим секунды
    } else
        $formattime .= $time . ' ' . __('sec.') . ' ';
    
    return __('after') . ' ' . $formattime;

}
/**
 * Format file size from bytes to K|M|G
 *
 * @param int $size
 * @return string - simple size with letter
 */
function getSimpleFileSize($size) {
	$size = intval($size);
	if (empty($size)) return '0 B';
	
	if (Config::read('IEC60027-2')==1) {
		$ext = array('B', 'KiB', 'MiB', 'GiB');
	} else {
		$ext = array('B', 'KB', 'MB', 'GB');
	}
	$i = 0;
	
	while (($size / 1024) > 1) {
		$size = $size / 1024;
		$i++;
	}

	
	$size = round($size, 2) . ' ' . $ext[$i];
	return $size;
}



/**
 * Return count registered users. 
 * Cache results.
 */
function getAllUsersCount() {
	$Register = Register::getInstance();
    $FpsDB = $Register['DB'];

    $Cache = new Cache;
	$Cache->lifeTime = 3600;
	$Cache->prefix = 'statistic';
	
	if ($Cache->check('cnt_registered_users')) {
		$cnt = $Cache->read('cnt_registered_users');
	} else {
		$cnt = $FpsDB->select('users', DB_COUNT);
		$Cache->write($cnt, 'cnt_registered_users', array());
	}
	
	unset($Cache);
	return (!empty($cnt)) ? intval($cnt) : 0;
}

/**
 * Clean getAllUsersCount() cache
 */
function cleanAllUsersCount() {
	$Cache = new Cache;
	$Cache->lifeTime = 3600;
	$Cache->prefix = 'statistic';
	if ($Cache->check('cnt_registered_users')) {
		$Cache->remove('cnt_registered_users');
	}
}


/** 
 * Get users that born today 
 */
function getBornTodayUsers() {
	$Register = Register::getInstance();
    $FpsDB = $Register['DB'];
	$file = ROOT . '/sys/logs/today_born.dat';
	

	if (!file_exists($file) || (filemtime($file) + 3600) < time()) {
		$today_born = $FpsDB->select('users', DB_ALL, array(
			'cond' => array(
				"concat(`bmonth`,`bday`) ='".date("nj")."'",
			),
		));
		file_put_contents($file, serialize($today_born));
	} else {
		$today_born = file_get_contents($file);
		if (!empty($today_born)) $today_born = unserialize($today_born);
	}
	
	if (count($today_born) < 1) return array();
	return $today_born;
}


/**
 * Function for safe and get referer
 */
function setReferer() {
	if (!empty($_SERVER['HTTP_REFERER']) 
	&& preg_match('#^http://([^/]+)/(.+)#', $_SERVER['HTTP_REFERER'], $match)) {
		if (!empty($match[1]) && !empty($match[2]) && $match[1] == $_SERVER['SERVER_NAME']) {
			$_SESSION['redirect_to'] = $match[2];
		}
	}
}
function getReferer() {
	$redirect_to = get_url('/');
	
	if (isset($_SESSION['redirect_to'])) {
		$redirect_to = get_url('/' . $_SESSION['redirect_to']);
		unset($_SESSION['redirect_to']);
		
	} else if (!empty($_SERVER['HTTP_REFERER']) 
	&& preg_match('#^http://([^/]+)/(.+)#', $_SERVER['HTTP_REFERER'], $match)) {
		if (!empty($match[1]) && !empty($match[2]) && $match[1] == $_SERVER['SERVER_NAME']) {
			$redirect_to = get_url('/' . $match[2]);
		}
	}
	
	return $redirect_to;
}



/**
 * Get age from params
 *
 * @param int $y - year
 * @param int $m - month
 * @param int $d - day
 * @return int
 */
function getAge($y = 1970, $m = 1, $d = 1) {
	$y = intval($y); $m = intval($m); $d = intval($d);
	
	if ($y < 1970 || $y > 2010) $y = 1970;
	if ($m < 1 || $m > 12) $m = 1;
	if ($d < 1 || $d > 31) $d = 1;
	$m = str_pad($m, 2, 0, STR_PAD_LEFT);
	$d = str_pad($d, 2, 0, STR_PAD_LEFT);
	
	$btime = mktime(0, 0, 0, $m, $d, $y);
	$age_in_sec = time() - $btime;
	$age = floor($age_in_sec / 31536000);
	
	return $age;
}




/**
 * Check and return order param
 */
function getOrderParam($class_name) {
	$order = (!empty($_GET['order'])) ? trim($_GET['order']) : '';
	
	switch ($class_name) {
		case 'FotoModule':
		case 'StatModule':
		case 'NewsModule':
			$allowed_keys = array('title', 'views', 'date', 'comments');
			$default_key = 'date';
			break;
		case 'LoadsModule':
			$allowed_keys = array('title', 'views', 'date', 'comments', 'downloads');
			$default_key = 'date';
			break;
		case 'ForumModule':
			$allowed_keys = array('title', 'time', 'last_post', 'posts', 'views');
			$default_key = 'last_post';
			break;
		case 'UsersModule':
			$allowed_keys = array('puttime', 'last_visit', 'name', 'rating', 'posts', 'status', 'warnings', 'city', 'jabber', 'byear', 'pol');
			$default_key = 'puttime';
			break;
	}
	
	if (empty($order) && empty($default_key)) return false;
	else if (empty($order) && !empty($default_key)) $out = $default_key;
	else {
		if (!empty($allowed_keys) && in_array($order, $allowed_keys)) {
			$out = $order;
		} else {
			$out = $default_key;
		}
	}
	
	return (!empty($_GET['asc'])) ? $out . ' ASC' : $out . ' DESC';
}
 


/**
 * CRON simulyation
 */
function fpsCron($func, $interval) {
	$cron_file = ROOT . '/sys/tmp/' . md5($func) . '_cron.dat';
	if (file_exists($cron_file)) {
		$extime = file_get_contents($cron_file);
		if (!empty($extime) && is_numeric($extime) && $extime > time()) {
			return;
		}
	}

	if (function_exists($func)) {
		file_put_contents($cron_file, (time() + intval($interval)));
		call_user_func($func);
	}
}
 
 
 
 
/**
 * Launch auto sitemap generator
 */
function createSitemap() {
	include_once ROOT . '/sys/inc/sitemap.class.php';
	$obj = new FpsSitemapGen;
	$obj->createMap();
}




// Автоподстановка значений Host и Sitemap
function createRobots() {
    $robots = array();
    $file = file(ROOT . "/robots.txt");

    if (isset($file) and is_array($file))
        foreach ($file as $buffer)
            if (substr($buffer,0,4)!='Host' and substr($buffer,0,7)!='Sitemap')
                $robots[] = $buffer;
    $robots[] = 'Host: '.$_SERVER['HTTP_HOST']."\n";
    $robots[] = 'Sitemap: http://'.$_SERVER['HTTP_HOST'].'/sitemap.xml';

    $str = '';
    foreach($robots as $line){
        $str .= $line;
    }
    file_put_contents(ROOT . "/robots.txt", $str);
}




function purgePreview() {
    $Register = Register::getInstance();
    if ($Register['Config']->read('use_local_preview', $params[0])) {
        $size_x = $Register['Config']->read('img_size_x', $params[0]);
        $size_y = $Register['Config']->read('img_size_y', $params[0]);
    } else {
        $size_x = $Register['Config']->read('img_size_x');
        $size_y = $Register['Config']->read('img_size_y');
    }

    // Min allowed size
    if ($size_x < 150) $size_x = 150;
    if ($size_y < 150) $size_y = 150;

    // Purge and make dirs
    $tmpdir = ROOT . '/sys/tmp/img_cache/'.$size_x.'x'.$size_y. '/';
    if (file_exists(ROOT . '/sys/tmp/img_cache/'))
        foreach(glob(ROOT . '/sys/tmp/img_cache/*/', GLOB_ONLYDIR) as $dir)
            if ($dir != $tmpdir)
                _unlink($dir);
}




/**
 * Create human like URL.
 * Get title of material and create url
 * from this title. OR create simple URL, if hlu is off.
 * 
 * @param array $materila
 * @param string $module
 * @return string 
 */
function entryUrl($material, $module) {
    $matId = $material->getId();
    $matTitle = $material->getTitle();

    $Register = Register::getInstance();
    return $Register['URL']->getEntryUrl($matId, $matTitle, $module);
}

function matUrl($matId, $matTitle, $module) {
    $Register = Register::getInstance();
    return $Register['URL']->getEntryUrl($matId, $matTitle, $module);
}




/**
 * Create captcha input field and image with 
 * security code.
 *
 */
function getCaptcha($name = false) {
	$kcaptcha = '/sys/inc/kcaptcha/kc.php?' . rand(rand(0, 1000), 999999);
	if (!empty($name)) $kcaptcha .= '&name=' . $name;
	$tpl = file_get_contents(ROOT . '/template/' . getTemplate() . '/html/default/captcha.html');
	return str_replace('{CAPTCHA}', $kcaptcha, $tpl);
}



/**
 * Work for language pack.
 * Open language file and return needed
 * string.
 *
 * @param int $key
 * @param string $tpl_lang_important - приоритет в совпадающих ключах в пользу шаблона.
 * @return string
 */

function __($key, $tpl_lang_important = false) {
    $language = getLang();
    if (empty($language) || !is_string($language)) $language = 'rus';

    $lang_file = ROOT . '/data/languages/' . $language . '.php';
    $tpl_lang_file = ROOT . '/template/' . getTemplate() .'/languages/' . $language . '.php';
    if (!file_exists($lang_file)) throw new Exception('Main language file not found');

    $lang = include $lang_file;
    
    // Если у шаблона есть свой файл локализации, загружаем его.
    $tpl_lang = array();
    if (file_exists($tpl_lang_file))
        $tpl_lang = include $tpl_lang_file;
    
    // Если у шаблона есть свой файл локализации,
    // если в нем есть такойже ключ, что и в основном,
    // а так же выбор значения ключа из него приоритетен,
    // выбираем ключ из файла при шаблоне.
    if (
    (array_key_exists($key, $lang) && !array_key_exists($key, $tpl_lang)) || // Если такого ключа в файле при шаблоне нет, а в основном файле есть
    (array_key_exists($key, $lang) && count($tpl_lang) && array_key_exists($key, $tpl_lang) && !$tpl_lang_important) //  Если есть и там и там определяется настройкой $tpl_lang_important
    )
        return $lang[$key];
    
    // Если в основном файле нет, а в файле при шаблоне есть.
    // Или при совпадении ключей настройка $tpl_lang_important равна true
    elseif (count($tpl_lang) && array_key_exists($key, $tpl_lang))
        return $tpl_lang[$key];
        
    return $key;
}


/**
 * Get the current user language
 */
function getLang() {
    // Если админка, то сессий использовать не нужно.
    if (strpos($_SERVER['REQUEST_URI'], WWW_ROOT . '/admin') === 0)
        return Config::read('language');
    else
        return (!empty($_SESSION['lang'])) ? $_SESSION['lang'] : Config::read('language');
}


/**
 * Get the permitted languages
 */
function getPermittedLangs() {
	$langs = Config::read('permitted_languages');
	if (!empty($langs)) {
		$langs = array_filter(explode(',', $langs));
		$langs = array_map(function($n){
			return trim($n);
		}, $langs);
		return $langs;
	} else {
		$lang_files = glob(ROOT . '/data/languages/*.php');
		$langs = array();
		if (!empty($lang_files)) {
			foreach($lang_files as $lang_file) {
				$lang = substr(substr(strrchr($lang_file, '/'), 1), 0, -4);
				$langs[] = $lang;
			}
		}
	}

	return $langs;
}






/**
 * Uses for valid create HTML tag IMG
 * and fill into him correctli url.
 * When you use this function you
 * mustn't wory obout Fapos install
 * into subdri or SUBDIRS.
 * ALso if we wont change class of IMG or etc,
 * we change this only here and this changes apply
 * for evrywhere.
 *
 * @param string $url
 * @param array $params
 * @param boolean $notRoot
 * @return string HTML link
 */
function get_img($url, $params = array(), $notRoot = false) {
	$additional = '';
	if (!empty($params) && is_array($params)) {
		foreach($params as $key => $value) {
			$additional .= h($key) . '="' . h($value) . '" ';
		}
	}
	return '<img  ' . $additional . 'src="' . get_url($url, $notRoot) . '" />';
}


/**
 * Uses for valid create url.
 * When you use this function you
 * mustn't wory obout Fapos install
 * into subdri or SUBDIRS.
 * This function return url only from root (/)
 * But you can send $notRoot and get url from not root.
 *
 * @param string $url
 * @param boolean $notRoot
 * @return string url
 */
function get_url($url, $notRoot = false) 
{
	if ($notRoot) return Pather::parseRoutes($url);
	$url = '/' . WWW_ROOT . $url;
	// May be collizions
	$url = str_replace('//', '/', $url);
	return Pather::parseRoutes($url);
}



/**
 * Uses for valid create HTML tag A
 * and fill into him correctli url.
 * When you use this function you
 * mustn't wory obout Fapos install
 * into subdri or SUBDIRS.
 * ALso if we wont change class of A or etc,
 * we change this only here and this changes apply
 * for evrywhere.
 *
 * @param string $ankor
 * @param string $url
 * @param array $params
 * @param boolean $notRoot
 * @param boolean $translate - пропускать ли $ancor через __($ancor)
 * @return string HTML link
 */
function get_link($ankor, $url, $params = array(), $notRoot = false, $translate = false) {
	$additional = '';
	if (!empty($params) && is_array($params)) {
		foreach($params as $key => $value) {
			$additional .= h($key) . '="' . h($value) . '" ';
		}
	}
	$link = '<a ' . $additional . 'href="' . get_url($url, $notRoot) . '">' . ($translate ? __($ankor) : $ankor) . '</a>';
	return $link;
}


/**
 * doing hard redirect
 * Send header and if header do not
 * work stop script and die. Better redirect
 * user to another page but if can't doing this
 * better stop script.
 */
function redirect($url, $header = 302) {
	
	$allowed_headers = array(301, 302);
	if (!in_array($header, $allowed_headers)) $header = 301;


	header('Location: ' . get_url($url), TRUE, $header);
	// :)
	die() or exit();
}



function createOptionsFromParams($offset, $limit, $selected = false) {
	$output = '';
	for ($i = $offset; $i <= $limit; $i++) {
		$select = ($selected !== false && $i == $selected) ? ' selected="selected"' : '';
		$output .= '<option value="' . $i . '"' . $select . '>' . $i . '</option>';
	}
	return $output;
}



/*
* return who online
* also we have analog in statistic module
*/
function getWhoOnline() {
	$path = ROOT . '/sys/logs/counter_online/online.dat';
	$users = 0;
	$quests = 0;
	$all = 0;
	
	if (file_exists($path) && is_readable($path)) {
		$data = unserialize(file_get_contents($path));
		$users = count($data['users']);
		$quests = count($data['guests']);
		$all = ($quests + $users);
	}
	
	return array('users' => $users, 'guests' => $quests, 'all' => $all);
}


/**
 * Return online users list
 */
function getOnlineUsers() {
	$path = ROOT . '/sys/logs/counter_online/online.dat';
	
	if (file_exists($path) && is_readable($path)) {
		$data = unserialize(file_get_contents($path));
		$users = $data['users'];
	}
	
	return $users;
}


/**
 * Get overal stats by key
 */
function getOveralStat($key = false) {
	$path = ROOT . '/sys/logs/overal_stats.dat';
	
	if (file_exists($path) && is_readable($path)) {
		$data = unserialize(file_get_contents($path));
		if (!empty($key)) {
			return (isset($data[$key])) ? $data[$key] : false;
		}
		return $data;
	}
	
	return false;	
}


/**
* touch and create dir
*/
function touchDir($path, $chmod = 0777) {
	if (!file_exists($path)) {
		mkdir($path, $chmod, true);
		chmod($path, $chmod);
	}
	return true;
}


/**
* print visibility param value
* @param string or array
*/
function pr($param) {
	echo '<pre>' . print_r($param, true) . '</pre>';
}


/**
* short version "htmlspecialchars()"
* @param string or array
*/
function h($param) {

	if (!is_array($param)) {
		$param = htmlspecialchars($param);
		$symbols = array(
			'&#125;' => '&amp;#125;',
			'&#123;' => '&amp;#123;',
		);
		return str_replace($symbols, array_keys($symbols), $param);
	}
	
	if (is_array($param)) {
		foreach ($param as $key => $value) {
			$param[$key] = h($value);
		}
		
		return $param;
	}
	
	return false;
}


/**
* @return timestamp on microseconds
*/
function getMicroTime() { 
    list($usec, $sec) = explode(" ", microtime()); 
    return ((float)$usec + (float)$sec); 
} 


/**
* for tests and dumps
*/
function dumpVar($var) {
	$f = fopen(ROOT . '/dump.dat', 'a+');
	fwrite($f, $var . "\n");
	fclose($f);
}


/**
 * mysql_real_escape_string copy
 */
function resc($str) {
    $Register = Register::getInstance();
    return $Register['DB']->escape($str);
}



function strips(&$param) {
	if (is_array($param)) {
		foreach($param as $k=>$v) {
			strips($param[$k]);
		}
	} else {
		$param = stripslashes($param);
		//$param = utf8Filter($param);
	}
}

/**
* cut all variables that not UTF-8
*/
function utf8Filter($str) {
	if (!preg_match('#.{1}#us', $str)) return '';
	else return $str;
}



function _unlink($path) {
    if(is_file($path)) return unlink($path);

    if(!is_dir($path)) return;
    $dh=opendir($path);
    if ($dh === False) return;
    while (false!==($file=readdir($dh))) {
        if($file=='.'||$file=='..') continue;
        _unlink($path."/".$file);
    }
    closedir($dh);

    return rmdir($path);
}


function memoryUsage($base_memory_usage) {
    printf("Bytes diff: %s<br />\n", getSimpleFileSize(memory_get_usage() - $base_memory_usage));
}

/**
* Get correct name of template for current user
*/
function getTemplate()
{
	
    
    if (isset($_SESSION['user']) && 
	   isset($_SESSION['user']['template']) && // Проверяем, чтобы брать имя шаблона из сессии юзера, и проверяем есть ли папка с таким шаблоном
	   !empty($_SESSION['user']['template']) && 
       file_exists(ROOT . '/template/' . $_SESSION['user']['template'])) {
            
            $template = $_SESSION['user']['template'];
            
    } else {
        $_SESSION['user']['template'] = Config::read('template');
        $template = $_SESSION['user']['template'];
    }
        
    
    $template = Plugins::intercept('select_template', $template);
	return $template;
}

/**
 * Get either a Gravatar URL or complete image tag for a specified email address.
 *
 * @param string $email The email address
 * @param string $s Size in pixels, defaults to 80px [ 1 - 2048 ]
 * @param string $d Default imageset to use [ 404 | mm | identicon | monsterid | wavatar ]
 * @param string $r Maximum rating (inclusive) [ g | pg | r | x ]
 * @return String containing either just a URL or a complete image tag
 */
function getGravatar($email, $s = 120, $d = 'mm', $r = 'g') {
	$url = 'http://www.gravatar.com/avatar/' . md5(strtolower(trim($email))) . ".png?s=$s&d=$d&r=$r";
	return $url;
}

function getAvatar($id_user = null, $email_user = null) {
	$def = get_url('/template/' . getTemplate() . '/img/noavatar.png');
	
	if (isset($id_user) && $id_user > 0) {
		if (is_file(ROOT . '/data/avatars/' . $id_user . '.jpg')) {
			return get_url('/data/avatars/' . $id_user . '.jpg');
		} else {
			if (Config::read('use_gravatar', 'users')) {
				if (!isset($email_user)) {
					$Register = Register::getInstance();
					$usersModel = $Register['ModManager']->getModelInstance('Users');
					$user = $usersModel->getById($id_user);
					if ($user) {
						$email_user = $user->getEmail();
					} else {
						return $def;
					}
				}
				return getGravatar($email_user);
			} else {
				return $def;
			}
		}
	} else {
		return $def;
	}
}

function checkPassword($md5_password, $password) {
	$check_password = false;
	if (strpos($md5_password, '$1$') === 0 && CRYPT_MD5 == 1) {
		$check_password = (crypt($password, $md5_password) === $md5_password);
	} else {
		$check_password = (md5($password) === $md5_password);
	}
	return $check_password;
}

function md5crypt($password){
	$Register = Register::getInstance();
	if ($Register['Config']->read('use_md5_salt', 'users') == 1) {
		$alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
		$salt = '';
		for($i = 0; $i < 4; $i++) {
			$salt .= $alphabet[rand(0, strlen($alphabet)-1)];
		}
		return crypt($password, '$1$' . $salt . '$');
	} else {
		return md5($password);
	}
}

function cmpText($a, $b) {
	if (is_array($a) && is_array($b) && isset($a['text']) && isset($b['text'])) {
		if ($a['text'] == $b['text']) {
			return 0;
		}
		return ($a['text'] < $b['text']) ? -1 : 1;
	} else {
		return 0;
	}
}

function checkAccess($params = null) {
	if (isset($params) && is_array($params)) {
		$Register = Register::getInstance();
		return $Register['ACL']->turn($params, false);
	}
	return false;
}

function checkUserOnline($user_id) {
	if (!$user_id || !is_numeric($user_id)) return false;
	$users_on_line = getOnlineUsers();
	return (isset($users_on_line) && isset($users_on_line[$user_id]));
}

function getOrderLink($params) {
	if (!$params || !is_array($params) || count($params) < 2) return '';
	$order = (!empty($_GET['order'])) ? strtolower(trim($_GET['order'])) : '';
	$new_order = strtolower($params[0]);
	$active = ($order === $new_order);
	$asc = ($active && isset($_GET['asc']));
	return '<a href="?order=' . $new_order . ($asc ? '' : '&asc=1') . '">' . $params[1] . ($active ? ' ' . ($asc ? '↑' : '↓') : '') . '</a>';
}

// обход массива и применение trim() для элементов
function atrim($old_array) {
	$new_array = array();
	foreach ($old_array as $element) {
		array_push($new_array, trim($element));
	}
	return $new_array;
}

/**
 * Similar to copy
 * @Recursive
 */
function copyr($source, $dest)
{
    // Simple copy for a file
    if (is_file($source)) {
        return copy($source, $dest);
    }
 
    // Make destination directory
    if (!is_dir($dest)) {
        mkdir($dest);
    }
   
    // If the source is a symlink
    if (is_link($source)) {
        $link_dest = readlink($source);
        return symlink($link_dest, $dest);
    }
 
    // Loop through the folder
    $dir = dir($source);
    while (false !== $entry = $dir->read()) {
        // Skip pointers
        if ($entry == '.' || $entry == '..') {
            continue;
        }
 
        // Deep copy directories
        if ($dest !== "$source/$entry") {
            copyr("$source/$entry", "$dest/$entry");
        }
    }
 
    // Clean up
    $dir->close();
    return true;
}

/**
 * Find all files in directory
 * @Recursive
 */
function getDirFiles($path) {
    $ret = array();
    $dir_iterator = new RecursiveDirectoryIterator($path);
    $iterator = new RecursiveIteratorIterator($dir_iterator, RecursiveIteratorIterator::SELF_FIRST);

    foreach ($iterator as $file) {
        if($file->isFile()) $ret[] = str_replace(ROOT, '', (string)$file);
    }

    return $ret;
}