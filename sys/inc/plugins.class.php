<?php
/**
* @project    Atom-M CMS
* @package    Plugins Class
* @url        http://cms.modos189.ru
*/


class Plugins {

    public static $map = array();
    private $errors = array();
    private $files = array();

    public function __construct() {
        $dirs = glob(ROOT . '/plugins/*');


        if (!empty($dirs)) {
            foreach ($dirs as $dir) {
                if (file_exists($dir . '/config.json')) {
                    $config = json_decode(file_get_contents($dir . '/config.json'), true);
                    if (!empty($config['points'])) {
                        if (is_string($config['points'])) {
                            if (empty(self::$map[$config['points']])) self::$map[$config['points']] = array();
                            self::$map[$config['points']][] = $dir;
                        } else if (is_array($config['points'])) {
                            foreach ($config['points'] as $point) {
                                if (empty(self::$map[$point])) self::$map[$point] = array();
                                self::$map[$point][] = $dir;
                            }
                        }
                    } else {
                        $hooks = array('before_pather', 'before_call_module', 'before_parse_layout', 'before_print_page', 'before_smiles_parse', 'before_view', 'after_pather');
                        foreach ($hooks as $hook) {
                            if (strpos($dir, $hook) !== false) {
                                if (empty(self::$map[$hook])) self::$map[$hook] = array();
                                self::$map[$hook][] = $dir;
                            }
                        }
                    }
                }
            }
        }
    }

    public function getErrors() {
        return $this->errors;
    }

    public function getFiles() {
        return $this->files;
    }

    public function install($filename, $dir = False) {
        $src = ROOT . '/sys/tmp/' . $filename;
        $dest = ROOT . '/sys/tmp/install_plugin/';

        Zip::extractZip($src, $dest);
        if (!file_exists($dest)) {
            $this->errors = __('Some error occurred');
            return false;
        }

        $tmp_plugin_path = glob($dest . '*', GLOB_ONLYDIR);
        $tmp_plugin_path = $tmp_plugin_path[0];
        $plugin_basename = substr(strrchr($tmp_plugin_path, '/'), 1);
        $plugin_basename = (isset($dir)) ? $dir : $plugin_basename;
        $plugin_path = ROOT . '/plugins/' . $plugin_basename;


        copyr($tmp_plugin_path, ROOT . '/plugins/'.$plugin_basename);
        $this->files = getDirFiles($plugin_path);


        $conf_pach = $plugin_path . '/config.json';
        if (file_exists($conf_pach)) {
            $config = json_decode(file_get_contents($conf_pach), true);


            include_once $plugin_path . '/index.php';
            if (isset($config['className']))
                $className = $config['className'];
            else
                $className = $plugin_basename;

            if (!class_exists($className))
                return false;

            $obj = new $className(null);

            if (method_exists($obj, 'install')) {
                if ($obj->install() == False) {
                    $this->errors = __('Some error occurred');
                    return false;
                }
            }

            $config['active'] = 1;
            file_put_contents($conf_pach, json_encode($config));
        }

        _unlink($src);
        _unlink($dest);

        return true;
    }

    public function foreignUpload($url) {
        $headers = get_headers($url,1);
        if (!empty($headers['Content-Disposition']))
            preg_match('#filename="(.*)"#iU', $headers['Content-Disposition'], $matches);
        $filename = (isset($matches[1])) ? $matches[1] : basename($url);

        if (copy($url, ROOT . '/sys/tmp/'.$filename)) {
            return $filename;
        } else {
            $this->errors = __('Some error occurred');
            return false;
        }
    }

    public function localUpload($field) {
        if (empty($_FILES[$field]['name'])) {
            $this->errors = __('File not found');
            return false;
        }

        $ext = strrchr($_FILES[$field]['name'], '.');
        if (strtolower($ext) !== '.zip') {
            $this->errors = __('Wrong file format');
            return false;
        }

        $filename = $_FILES[$field]['name'];

        if (move_uploaded_file($_FILES[$field]['tmp_name'], ROOT . '/sys/tmp/'.$filename)) {
            return $filename;
        } else {
            $this->errors = __('Some error occurred');
            return false;
        }
    }

    /**
     * Find plugin by key and launch his
     *
     * @param string $key
     * @param mixed $params
     */
    public static function intercept($key, $params = array()) {
        if (!empty(self::$map[$key])) {
            foreach (self::$map[$key] as $plugin) {
                if (!is_dir($plugin)) continue;
                $plugin_basename = mb_substr($plugin, mb_strrpos($plugin, "/") + 1);

                $pl_conf = file_get_contents($plugin . '/config.json');
                $pl_conf = json_decode($pl_conf, 1);
                if (empty($pl_conf['active'])) continue;


                include_once $plugin . '/index.php';

                if (isset($pl_conf['className']))
                    $className = $pl_conf['className'];
                else
                    $className = $plugin_basename;

                if (!class_exists($className))
                    continue;

                $pl_obj = new $className($params);
                $pl_obj->plugin_path = get_url('/plugins/' . $plugin_basename);
                $params = $pl_obj->common($params);

                // Processign tag {{ plugin_path }} (Для совместимости со старыми плагинами)
                if (is_string($params) && mb_strpos($params, 'plugin_path') !== false) {
                    $path = get_url('/plugins/' . $plugin_basename);
                    $params = preg_replace('#{{\s*plugin_path\s*}}#i', $path, $params);
                }
            }
        }
        return $params;
    }
}