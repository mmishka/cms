<?php
// проверка прав
if (!$this->ACL->turn(array($this->module, 'edit_comments'), false)
    && (!$this->ACL->turn(array($this->module, 'edit_my_comments'), false))) {

    return $this->showInfoMessage(__('Permission denied'), $this->getModuleURL());
}

$id = (!empty($id)) ? (int)$id : 0;
if ($id < 1) return $this->showInfoMessage(__('Unknown error'), $this->getModuleURL(), 1);


$commentsModel = $this->Register['ModManager']->getModelInstance('Comments');
if (!$commentsModel) return $this->showInfoMessage(__('Some error occurred'), $this->getModuleURL(), 1);
$comment = $commentsModel->getById($id);
if (!$comment) return $this->showInfoMessage(__('Comment not found'), $this->getModuleURL(), 1);


if (strtotime($comment->getEditdate()) > strtotime($comment->getDate()))
    $lasttime = strtotime($comment->getEditdate());
 else
    $lasttime = strtotime($comment->getDate());
$raw_time_mess = $lasttime - time() + Config::read('raw_time_mess');
if ($raw_time_mess <= 0) $raw_time_mess = false;

// дополнительная проверка прав
if (!$this->ACL->turn(array($this->module, 'edit_comments'), false)
    && (!empty($_SESSION['user']['id']) && $post->getUser_id() == $_SESSION['user']['id']
    && $this->ACL->turn(array($this->module, 'edit_my_comments'), false)
    && (Config::read('raw_time_mess') == 0 or $raw_time_mess)) === false) {

    return $this->showInfoMessage(__('Permission denied'), $this->getModuleURL());
}


/* cut and trim values */
if ($comment->getUser_id() > 0) {
	$name = $comment->getName();
} else {
	$name = mb_substr($_POST['login'], 0, 70);
	$name = trim($name);
}


$mail = '';
$message = (!empty($_POST['message'])) ? $_POST['message'] : '';
$message = trim($message);


$error = '';
$valobj = $this->Register['Validate'];

$max_lenght = Config::read($this->module, 'comment_lenght');
if ($max_lenght <= 0)
    $max_lenght = 500;
$min_lenght = 2;

if (empty($name))
    $error .= '<li>' . __('Empty field "login"') . '</li>' . "\n";
elseif (!$valobj->cha_val($name, V_TITLE))
    $error .= '<li>' . __('Wrong chars in field "login"') . '</li>' . "\n";
if (empty($message))
    $error .= '<li>' . __('Empty field "text"') . '</li>' . "\n";
elseif (mb_strlen($message) > $max_lenght)
    $error .= '<li>' . sprintf(__('Very big "comment"'), $max_lenght) . '</li>' . "\n";
elseif (mb_strlen($message) < $min_lenght)
    $error .= '<li>' . sprintf(__('Very small "comment"'), $min_lenght) . '</li>' . "\n";
	
/* if an error */
if (!empty($error)) {
	$_SESSION['editCommentForm'] = array();
	$_SESSION['editCommentForm']['errors'] = '<p class="errorMsg">' . __('Some error in form') . '</p>'
		. "\n" . '<ul class="errorMsg">' . "\n" . $error . '</ul>' . "\n";
	$_SESSION['editCommentForm']['message'] = $message;
	$_SESSION['editCommentForm']['name'] = $name;
	return $this->showInfoMessage($_SESSION['editCommentForm']['errors'], $this->getModuleURL('/edit_comment_form/' . $id), 1);
}


//remove cache
$this->Cache->clean(CACHE_MATCHING_TAG, array('module_' . $this->module, 'record_id_' . $comment->getEntity_id()));
$this->DB->cleanSqlCache();


// Update comment
$comment->setMessage($message);
$comment->setEditdate(new Expr('NOW()'));
if ($name) $comment->setName($name);
$comment->save();


if ($this->Log) $this->Log->write('editing comment for ' . $this->module, $this->module . ' id(' . $comment->getEntity_id() . '), comment id(' . $id . ')');
return $this->showInfoMessage(__('Operation is successful'), $this->getModuleURL('/view/' . $comment->getEntity_id()));
?>