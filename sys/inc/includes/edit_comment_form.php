<?php
// проверка прав
if (!$this->ACL->turn(array($this->module, 'edit_comments'), false)
    && (!$this->ACL->turn(array($this->module, 'edit_my_comments'), false))) {

    return $this->showInfoMessage(__('Permission denied'), $this->getModuleURL());
}

$id = (!empty($id)) ? (int)$id : 0;
if ($id < 1) return $this->showInfoMessage(__('Unknown error'), $this->getModuleURL(), 1);


$commentsModel = $this->Register['ModManager']->getModelInstance('Comments');
if (!$commentsModel) return $this->showInfoMessage(__('Some error occurred'), $this->getModuleURL());
$comment = $commentsModel->getById($id);
if (!$comment) return $this->showInfoMessage(__('Comment not found'), $this->getModuleURL());


if (strtotime($comment->getEditdate()) > strtotime($comment->getDate()))
    $lasttime = strtotime($comment->getEditdate());
 else
    $lasttime = strtotime($comment->getDate());
$raw_time_mess = $lasttime - time() + Config::read('raw_time_mess');
if ($raw_time_mess <= 0) $raw_time_mess = false;

// дополнительная проверка прав
if (!$this->ACL->turn(array($this->module, 'edit_comments'), false)
    && ($this->ACL->turn(array($this->module, 'edit_my_comments'), false)
    && (Config::read('raw_time_mess') == 0 or $raw_time_mess)) === false) {

    return $this->showInfoMessage(__('Permission denied'), $this->getModuleURL());
}


// Categories tree
$entity = $this->Model->getById($comment->getEntity_id());
if ($entity && $entity->getCategory_id()) {
	$this->categories = $this->_getCatsTree($entity->getCategory_id());
} else {
	$this->categories = $this->_getCatsTree();
}


$markers = array();
$markers['disabled'] = ($comment->getUser_id()) ? ' disabled="disabled"' : '';


// Если при заполнении формы были допущены ошибки
if (isset($_SESSION['editCommentForm'])) {
	$errors   = $_SESSION['editCommentForm']['errors'];
	$message  = $_SESSION['editCommentForm']['message'];
	$name     = $_SESSION['editCommentForm']['name'];
	unset($_SESSION['editCommentForm']);
} else {
	$errors = '';
	$message = $comment->getMessage();
	$name    = $comment->getName();
}


$markers['action'] = get_url($this->getModuleURL('/update_comment/' . $id));
$markers['errors'] = $errors;
$markers['name'] = h($name);
$markers['message'] = h($message);

// nav block
$navi = array();
$navi['navigation'] = get_link(__('Home'), '/') . __('Separator')
    . get_link(h($this->module_title), $this->getModuleURL()) . __('Separator')
    . __('Edit comment');
$this->_globalize($navi);

$source = $this->render('editcommentform.html', array(
	'form' => $markers,
	'comment' => $comment,
));
$this->comments = '';

return $this->_view($source);
