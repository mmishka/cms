<?php
//turn access
$this->ACL->turn(array($this->module, 'add_comments'));
$id = (int)$id;
if ($id < 1) return $this->showInfoMessage(__('Unknown error'), $this->getModuleURL(), 1);


$target_new = $this->Model->getById($id);
if (!$target_new) return $this->showInfoMessage(__('Unknown error'), $this->getModuleURL(), 1);
if (!$target_new->getCommented()) return $this->showInfoMessage(__('Comments are denied here'), $this->getModuleURL('/view/' . $id), 1);

/* cut and trim values */
if ($_SESSION['user']['id'] != 0) {
    $name = $_SESSION['user']['name'];
} else {
    if (isset($_POST['login'])) {
        $name = mb_substr($_POST['login'], 0, 70);
        $name = trim($name);
    } else {
        $name = '';
    }
}


$mail = '';
$message = trim($_POST['message']);
$ip      = (!empty($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : '';
$keystring = (isset($_POST['captcha_keystring'])) ? trim($_POST['captcha_keystring']) : '';


// Check fields
$error = '';
$valobj = $this->Register['Validate'];

$max_lenght = Config::read($this->module, 'comment_lenght');
if ($max_lenght <= 0)
    $max_lenght = 500;
$min_lenght = 2;

if (empty($name))
    $error .= '<li>' . __('Empty field "login"') . '</li>' . "\n";
elseif (!$valobj->cha_val($name, V_TITLE))  
    $error .= '<li>' . __('Wrong chars in field "login"') . '</li>' . "\n";
if (empty($message))
    $error .= '<li>' . __('Empty field "text"') . '</li>' . "\n";
elseif (mb_strlen($message) > $max_lenght)
    $error .= '<li>' . sprintf(__('Very big "comment"'), $max_lenght) . '</li>' . "\n";
elseif (mb_strlen($message) < $min_lenght)
    $error .= '<li>' . sprintf(__('Very small "comment"'), $min_lenght) . '</li>' . "\n";


// Check captcha if need exists	 
if (!$this->ACL->turn(array('other', 'no_captcha'), false)) {
	if (empty($keystring))                      
		$error .= '<li>' . __('Empty field "code"') . '</li>' . "\n";

	
	// Проверяем поле "код"
	if (!empty($keystring)) {
		// Проверяем поле "код" на недопустимые символы
		if (!$valobj->cha_val($keystring, V_CAPTCHA))
			$error = $error.'<li>' . __('Wrong chars in field "code"') . '</li>'."\n";									
		if (!isset($_SESSION['captcha_keystring'])) {
			if (file_exists(ROOT . '/sys/logs/captcha_keystring_' . session_id() . '-' . date("Y-m-d") . '.dat')) {
				$_SESSION['captcha_keystring'] = file_get_contents(ROOT . '/sys/logs/captcha_keystring_' . session_id() . '-' . date("Y-m-d") . '.dat');
				@_unlink(ROOT . '/sys/logs/captcha_keystring_' . session_id() . '-' . date("Y-m-d") . '.dat');
			}
		}
		if (!isset($_SESSION['captcha_keystring']) || $_SESSION['captcha_keystring'] != $keystring)
			$error = $error.'<li>' . __('Wrong protection code') . '</li>'."\n";
	}
	unset($_SESSION['captcha_keystring']);
}


/* if an errors */
if (!empty($error)) {
	$_SESSION['addCommentForm'] = array();
	$_SESSION['addCommentForm']['errors'] = '<p class="errorMsg">' . __('Some error in form') . '</p>' .
		"\n" . '<ul class="errorMsg">' . "\n" . $error . '</ul>' . "\n";
	$_SESSION['addCommentForm']['name'] = $name;
	$_SESSION['addCommentForm']['message'] = $message;
	return $this->showInfoMessage($_SESSION['addCommentForm']['errors'], $this->getModuleURL('/view/' . $id), 1);
}


$id_user = $_SESSION['user']['id'];
/* SPAM DEFENCE */
if (isset($_SESSION['unix_last_post']) and (time()-$_SESSION['unix_last_post'] < 10)) {
	return $this->showInfoMessage(__('You can not add messages so often'), $this->getModuleURL('/view/' . $id), 1);
} else {
	$_SESSION['unix_last_post'] = time();
}


/* remove cache */
$this->Cache->clean(CACHE_MATCHING_TAG, array('module_' . $this->module, 'record_id_' . $id));
$this->DB->cleanSqlCache();


$commentsModel = $this->Register['ModManager']->getModelInstance('Comments');
if (!$commentsModel) return $this->showInfoMessage(__('Some error occurred'), $this->getModuleURL());

$prev_comm = $commentsModel->getFirst(array(
        'entity_id' => $id,
    ), array(
        'order' => 'date DESC, id DESC',
));

$gluing = true;
if ($prev_comm) {
    if (strtotime($prev_comm->getEditdate()) > strtotime($prev_comm->getDate()))
        $lasttime = strtotime($prev_comm->getEditdate());
    else
        $lasttime = strtotime($prev_comm->getDate());
    $gluing = $lasttime > time() - Config::read('raw_time_mess');
    $prev_post_author = $prev_comm->getUser_id();
    if (empty($prev_post_author))
        $gluing = false;
    if ((mb_strlen($prev_comm->getMessage() . $message)) > $max_lenght)
        $gluing = false;
    if ($prev_post_author != $id_user || empty($id_user))
        $gluing = false;
} else {
    $gluing = false;
}


if ($gluing === true) {
    $message = $prev_comm->getMessage() . "\n\n" . sprintf(__('Added in time'), AtmOffsetDate(strtotime($prev_comm->getDate()))) . "\n\n" . $message;

    $prev_comm->setMessage($message);
    $prev_comm->setEditdate(new Expr('NOW()'));
    $prev_comm->save();

    if ($this->Log) $this->Log->write('adding comment to ' . $this->module, $this->module . ' id(*gluing)');
    return $this->showInfoMessage(__('Comment is added'), $this->getModuleURL('/view/' . $id));
} else {
    /* save data */	
    $data = array(
        'entity_id'   => $id,
        'name'     => $name,
        'message'  => $message,
        'ip'       => $ip,
        'user_id'  => $id_user,
        'date'     => new Expr('NOW()'),
        'mail'     => $mail,
        'module'   => $this->module,
        'editdate' => '0000-00-00 00:00:00',
        'premoder'     => 'confirmed',
    );

    if ($this->ACL->turn(array($this->module, 'comments_require_premoder'), false)) {
        $data['premoder'] = 'nochecked';
    }

    $className = $this->Register['ModManager']->getEntityName('Comments');
    $entityComm = new $className($data);
    if ($entityComm) {
        $entityComm->save();

        $entity = $this->Model->getById($id);
        if ($entity) {
            $entity->setComments($entity->getComments() + 1);
            $entity->save();

            if ($this->Log) $this->Log->write('adding comment to ' . $this->module, $this->module . ' id(' . $id . ')');

            if ($this->ACL->turn(array($this->module, 'comments_require_premoder'), false))
                $msg = $this->showInfoMessage(__('Comment will be available after validation'), $this->getModuleURL('/view/' . $id));
            else
                $msg = $this->showInfoMessage(__('Comment is added'), $this->getModuleURL('/view/' . $id));
            return $msg;
        }
    }
}
return $this->showInfoMessage(__('Some error occurred'), $this->getModuleURL('/view/' . $id), 1);
?>