<?php
/**
* Parse pages and data. Replaced snippets.
* Quote/unquote global tags.
*
* @project    Atom-M CMS
* @package    Document parser library
* @url        http://cms.modos189.ru
*/


class Document_Parser {

	/**
	 * @var object
	 */
	private $Cache;
	
	/**
	 * @var string 
	 */
	public $templateDir;

    /**
     * @var bool|int
     */
	private static $levels = false;

    /**
     * @var int
     */
    private $maxLevels = 3;

    /**
     * @var object
     */
    private $Register;



	/**
	 *
	 */
	public function __construct()
    {
        $this->Register = Register::getInstance();
		$this->Cache = new Cache;
		$this->Cache->prefix = 'block';
		$this->Cache->cacheDir = ROOT . '/sys/cache/blocks/';
		$this->Cache->lifeTime = 3600;
	}



    /**
     * @return mixed|string
     */
    public function getErrors()
    {
		$viewer = new Fps_Viewer_Manager();
        $outputContent = '';
        if (!empty($_SESSION['FpsForm']['errors'])) {
            $outputContent = $viewer->view('infomessage.html', array('message' => $_SESSION['FpsForm']['errors']));
        }
        return $outputContent;
    }


	/**
	* @param       string $page
	* @return      data with parsed snippets
	*/
	public function parseSnippet($page)
    {
        $SnippetsParser = new AtmSnippets($page);
        return $SnippetsParser->parse();
	}
	
	/**
	* @param       string $page
	* @return      data with parsed global tags
	*/
	public function getGlobalMarkers($page = '')
    {
        $Register = Register::getInstance();
		$markers = array();
		
		$markers['fps_server_name'] = $_SERVER['SERVER_NAME'];
        $markers['fps_request_url'] = $_SERVER['REQUEST_URI'];
		$markers['fps_wday'] = date("D");
		$markers['fps_wday_n'] = date("w");
		$markers['fps_date'] = date("d-m-Y");
		$markers['fps_time'] = date("H:i");
		$markers['fps_hour'] = date("G");
		$markers['fps_minute'] = date("i");
		$markers['fps_day'] = date("j");
		$markers['fps_month'] = date("n");
		$markers['fps_year'] = date("Y");
		
		$path = $Register['Config']->read('smiles_set');
		$path = (!empty($path) ? $path : 'atomm');
		$markers['smiles_set'] = $path;
		$path = ROOT . '/data/img/smiles/' . $path . '/info.php';
		include $path;
		if (isset($smilesList) && is_array($smilesList)) {
			$markers['smiles_list'] = (isset($smilesInfo) && isset($smilesInfo['show_count'])) ? array_slice($smilesList, 0, $smilesInfo['show_count']) : $smilesList;
		} else {
			$markers['smiles_list'] = array();
		}
		
		$markers['powered_by'] = 'Atom-M CMS';
		$markers['site_title'] = Config::read('site_title');
		
		$atm_user = $_SESSION['user'];
		unset($atm_user['passw']);
		if (isset($_SESSION['user']['name'])) {
			$atm_user['profile'] = get_url(getProfileUrl($_SESSION['user']['id']));
			$atm_user['id'] = $_SESSION['user']['id'];
			$atm_user['name'] = $_SESSION['user']['name'];
			$userGroup = $Register['ACL']->get_user_group($_SESSION['user']['status']);
			$atm_user['group'] = $userGroup['title'];

			$get_difference_time = (time() - strtotime($_SESSION['user']['puttime'])) / 86400;
			if ($get_difference_time < 0) $get_difference_time = 0;

            $atm_user['reg_days'] = round($get_difference_time);

            if (strstr($page, '{{ atm_user.avatar_url }}') or strstr($page, '{{ fps_user_avatar_url }}')) {
                $atm_user['avatar_url'] = getAvatar($_SESSION['user']['id']);
            } else {
                $atm_user['avatar_url'] = getAvatar();
            }

			$atm_user['unread_pm'] = 0;
            if (strstr($page, '{{ atm_user.unread_pm }}') or strstr($page, '{{ unread_pm }}')) {
                $usersModel = $this->Register['ModManager']->getModelInstance('Users');
                $res = $usersModel->getNewPmMessages($_SESSION['user']['id']);
                if ($res)
                    $atm_user['unread_pm'] = $res;
            }
		} else {
			$atm_user['profile'] = get_url('/users/add_form/');
			$atm_user['id'] = 0;
			$atm_user['name'] = 'Гость'; //TODO
			$atm_user['group'] = 'Гости';
			$atm_user['reg_days'] = '';
			$atm_user['unread_pm'] = '';
			$atm_user['avatar_url'] = getAvatar();
		}
        $atm_user['admin_access'] = ($Register['ACL']->turn(array('panel', 'entry'), false)) ? '1' : '0';

		$markers['atm_user'] = $atm_user;
		
		// TODO поудалять потом эти старые метки
		$markers['personal_page_link'] = $atm_user['profile'];
        $markers['fps_user_avatar_url'] = $atm_user['avatar_url'];
        $markers['fps_user_id'] = $atm_user['id'];
		$markers['fps_user_name'] = $atm_user['name'];
		$markers['fps_user_group'] = $atm_user['group'];
		$markers['fps_user_reg_days'] = $atm_user['reg_days'];
		$markers['unread_pm'] = $atm_user['unread_pm'];
		$markers['fps_admin_access'] = $atm_user['admin_access'];
		
		
		$online = getWhoOnline();
		$markers['all_online'] = ($online['users'] + $online['guests']);
		$markers['users_online'] = $online['users'];
		$markers['guests_online'] = $online['guests'];
		$markers['online_users_list'] = (!empty($_SESSION['online_users_list'])) ? $_SESSION['online_users_list'] : '';
		$markers['count_users'] = getAllUsersCount();
		
		$overal_stats = getOveralStat();
		$markers['max_online_all_time'] = (!empty($overal_stats['max_users_online'])) 
		? intval($overal_stats['max_users_online']) : 0;
		$markers['max_online_all_time_date'] = (!empty($overal_stats['max_users_online_date'])) 
		? h($overal_stats['max_users_online_date']) : 'Uncnown';
		
	
		if (Config::read('active', 'chat') and strstr($page, '{{ fps_chat }}')) {
			include_once ROOT . '/modules/chat/index.php';
			$markers['fps_chat'] = ChatModule::form();
			$markers['fps_chat'] .= ChatModule::add_form();
		}
		
		
		$markers['counter'] = get_url('/data/img/counter.png?rand=' . rand(0,999999));
		$markers['template_path'] = get_url('/template/' . getTemplate());
		$markers['www_root'] = WWW_ROOT;
        $markers['lang'] = getLang();
        $markers['langs'] = (count(getPermittedLangs()) >= 1) ? getPermittedLangs() : '';
		
		
		$markers['fps_rss'] = $this->getRss();
		
		if (false !== (strpos($page, '{{ mainmenu }}'))) {
			$markers['mainmenu'] = $this->builMainMenu();
		}
		
		// today borned users
		$today_born = getBornTodayUsers();
		$tbout = '';
		if (count($today_born) > 0) {
			$names = array();
			foreach ($today_born as $user) {
				$names[] = get_link($user['name'], getProfileUrl($user['id']));
			}
			$tbout = implode(', ', $names);
		}
		$markers['today_born_users'] = (!empty($tbout)) ? $tbout : __('No birthdays today');
		
		$markers['fps_users_groups'] = $Register['ACL']->getGroups();
		$markers['fps_users_edit'] = ($Register['ACL']->turn(array('users', 'edit_users'), false)) ? '1' : '0';
		
		return 	$markers;
	}
	
	
    /**
    * @return     list with RSS links
    */
    public function getRss()
    {
        $rss = '';
        $modules = glob(ROOT.'/modules/*');
        foreach ($modules as $module):
            if (is_dir($module)
                and preg_match('#/(\w+)$#i', $module, $module_name)
                and Config::read('active', $module_name[1])
                and Config::read('rss_'.$module_name[1], 'rss')):

                $rss .= get_img('/template/' . getTemplate() . '/img/rss_icon_mini.png') .
                    get_link(__(ucfirst($module_name[1]) . ' RSS'), '/'.$module_name[1].'/rss/') .
                    '<br />';

            endif;
        endforeach;
        return $rss;
    }
	
	

	/**
     * @return string
     *
	 * Build menu which creating in Admin Panel
	 */
	public function builMainMenu()
    {
		$menu_conf_file = ROOT . '/sys/settings/menu.dat';	
		if (!file_exists($menu_conf_file)) return false;
		$menudata = unserialize(file_get_contents($menu_conf_file));
	
		
		if (!empty($menudata) && count($menudata) > 0) {
			$out = $this->buildMenuNode($menudata, 'class="fpsMainMenu"');
		} else {
			return false;
		}
		return $out;
	}


    /**
     * @param  $node
     * @param string $class
     * @return string
     */
	public function buildMenuNode($node, $class = 'class="fpsMainMenu"')
    {
		$out = '<ul ' . $class . '>';
		foreach ($node as $point) {
			if (empty($point['title']) || empty($point['url'])) continue;
			if (!empty($point['sub']) && count($point['sub']) > 0) {
			   $subClass = 'class="fpsSubMenu"';
			} else {
			   $subClass = '';
			}
			$out .= '<li ' . $subClass . '>';
			
			
			$out .= $point['prefix'];
			$target = (!empty($point['newwin'])) ? ' target="_blank"' : '';
			$out .= '<a href="' . get_url($point['url']) . '"' . $target . '>' . $point['title'] . '</a>';
			$out .= $point['sufix'];
			
			if (!empty($point['sub']) && count($point['sub']) > 0) {
				$out .= $this->buildMenuNode($point['sub']);
			}
			
			$out .= '</li>';
		}
		$out .= '</ul>';
		return $out;
	}
	
}
