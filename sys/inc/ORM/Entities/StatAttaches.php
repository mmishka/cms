<?php
/**
* @project    Atom-M CMS
* @package    StatAttaches Entity
* @url        http://cms.modos189.ru
*/


/**
 *
 */
class StatAttachesEntity extends FpsEntity
{
	
	protected $id;
	protected $entity_id;
	protected $user_id;
	protected $attach_number;
	protected $filename ;
	protected $size;
	protected $date;
	protected $is_image;

	
	public function save()
	{
		$params = array(
			'entity_id' => intval($this->entity_id),
			'user_id' => intval($this->user_id),
			'attach_number' => intval($this->attach_number),
			'filename' => $this->filename,
			'size' => intval($this->size),
			'date' => $this->date,
			'is_image' => (!empty($this->is_image)) ? '1' : new Expr("'0'"),
		);
		if($this->id) $params['id'] = $this->id;
		$Register = Register::getInstance();
		return ($Register['DB']->save('stat_attaches', $params));
	}
	
	
	
	public function delete()
	{
		$path = ROOT . '/data/files/stat/' . $this->filename;
		if (file_exists($path)) unlink($path);

        if (Config::read('use_local_preview', 'stat')) {
            $preview = Config::read('use_preview', 'stat');
            $size_x = Config::read('img_size_x', 'stat');
            $size_y = Config::read('img_size_y', 'stat');
        } else {
            $preview = Config::read('use_preview');
            $size_x = Config::read('img_size_x');
            $size_y = Config::read('img_size_y');
        }
        $path = ROOT.'/sys/tmp/img_cache/'.$size_x.'x'.$size_y.'/stat/'.$this->filename;
        if (file_exists($path)) unlink($path);

		$Register = Register::getInstance();
		$Register['DB']->delete('stat_attaches', array('id' => $this->id));
	}
}